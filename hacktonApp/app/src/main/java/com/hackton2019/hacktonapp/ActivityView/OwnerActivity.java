package com.hackton2019.hacktonapp.ActivityView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.akash.RevealSwitch;
import com.akash.revealswitch.OnToggleListener;
import com.google.android.material.button.MaterialButton;
import com.hackton2019.hacktonapp.ClasesServices.ParkingsClass.Parking;
import com.hackton2019.hacktonapp.ConnectionServices.TinyDB;
import com.hackton2019.hacktonapp.HelperClass.Adapters.ParkingListAdapter;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogWait;
import com.hackton2019.hacktonapp.HelperClass.Navigation;
import com.hackton2019.hacktonapp.R;

import java.util.ArrayList;
import java.util.List;

public class OwnerActivity extends AppCompatActivity {


    LinearLayout containerList, containerInfo;
    Parking parking;
    public RecyclerView mRecycleView;
    public ParkingListAdapter mAdapter;
    public RecyclerView.LayoutManager mLayoutManager;

    DialogWait dialogWait = new DialogWait();
    List<Parking> parkings = new ArrayList<>();

    MaterialButton btnBack;

    RevealSwitch switchParking;

    TinyDB tinyDB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.owner_activity);
        init();
        obtainMyParks();
    }

    private void obtainMyParks() {
        showDialg();


        Thread threadA = new Thread(new Runnable() {
            @Override
            public void run() {
                parking = new Parking();
                parkings = parking.myParkings(getApplicationContext());

                OwnerActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (parking.isResponseState()){
                            Log.i("tamanio", "run: "+parkings.size());
                            builderList();
                            closeDialog();
                        }else {
                            closeDialog();
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.error_owner_partking),Toast.LENGTH_SHORT).show();
                            Navigation.navegate(Navigation.MAP_ACTIVITY,Navigation.FLAG_FULL,getApplicationContext());
                            finish();
                        }
                    }
                });
            }
        });
        threadA.start();


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.navegate(Navigation.MAP_ACTIVITY,Navigation.FLAG_FULL,getApplicationContext());
                finish();
            }
        });


        switchParking.setToggleListener(new OnToggleListener() {
            @Override
            public void onToggle(final boolean b) {
                Thread threadA = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (b){
                            parking = new Parking();
                            parking.setId(tinyDB.getString("parkingID"));
                            parking.lockParking(getApplicationContext(),"lock");
                        }else if (!b){
                            parking = new Parking();
                            parking.setId(tinyDB.getString("parkingID"));
                            parking.lockParking(getApplicationContext(),"unlock");
                        }else {
                            Log.i("errorLock", "run: Ocurrio un error");
                            Toast.makeText(getApplicationContext(),"No se pudo completar el proceso",Toast.LENGTH_SHORT).show();
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                switchParking.setEnable(b);
                            }
                        });
                    }
                });

                threadA.start();
            }
        });


    }

    @Override
    public void onBackPressed() {
        Navigation.navegate(Navigation.MAP_ACTIVITY,Navigation.FLAG_FULL,getApplicationContext());
        finish();
    }

    private void builderList() {
        mRecycleView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mAdapter = new ParkingListAdapter(parkings,getApplicationContext(),OwnerActivity.this);

        mRecycleView.setAdapter(mAdapter);
        mRecycleView.setLayoutManager(mLayoutManager);
    }


    private void init() {
        mRecycleView = findViewById(R.id.owner_recycle_view);
        containerInfo = findViewById(R.id.containerItemList);
        containerList = findViewById(R.id.containerList);

        btnBack = findViewById(R.id.btnBack);


        switchParking = findViewById(R.id.owner_main_parking_switch);

        tinyDB = new TinyDB(getApplicationContext());
    }

    private void closeDialog() {
        dialogWait.dismiss();
    }

    private void showDialg() {
        dialogWait.setCancelable(false);
        dialogWait.show(getSupportFragmentManager(),"Cargando");
    }
}
