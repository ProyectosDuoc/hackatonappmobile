package com.hackton2019.hacktonapp.ActivityView.ServiceParking;

import androidx.appcompat.app.AppCompatActivity;

import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.hackton2019.hacktonapp.ConnectionServices.TinyDB;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogSelectedCard;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogYesNo;
import com.hackton2019.hacktonapp.HelperClass.Navigation;
import com.hackton2019.hacktonapp.R;

import java.util.Calendar;

public class parkimetroActivity extends AppCompatActivity {

    private Chronometer timeParking;
    private ProgressBar progressBar;
    private long pauseOffset;
    private boolean running = false;
    private MaterialButton btnStop;
    private TextView textDateEnd;
    private TextView textCostPH;
    private TextView textCostEST;

    private TextView textPatente;
    int costoPH,costo10,Total;
    boolean firstTime,everyTen;
    long elapsedMillis;

    CountDownTimer timer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.parkimetro);
        init();

        timer = new CountDownTimer(600000, 1000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                timeParking.setVisibility(View.VISIBLE);
                startChronometer();
                timer = null;
            }
        }.start();

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DialogYesNo dialogInfo = new DialogYesNo();
                dialogInfo.parkimetro = parkimetroActivity.this;
                dialogInfo.setCancelable(false);
                dialogInfo.show(getSupportFragmentManager(),"SiNO");




            }
        });

        timeParking.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {

                try {
                    elapsedMillis = SystemClock.elapsedRealtime() -chronometer.getBase();
                    if(elapsedMillis > 3600000L){
                        chronometer.setFormat("0%s");
                    }else{
                        chronometer.setFormat("00:%s");
                    }

                    if (firstTime){
                        Total = costoPH/2;
                        Log.i("cobro", "onChronometerTick: Le empiezo a cobrar "+Total);
                        textCostEST.setText(getResources().getString(R.string.estimated)+" "+Total);
                        firstTime = false;
                        everyTen = true;
                    }else if (everyTen){
                        everyTen = false;
                        timer = new CountDownTimer(600000, 1000) {
                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                Total = Total+costo10;
                                Log.i("total", "onFinish: "+Total);
                                textCostEST.setText(getResources().getString(R.string.estimated)+" "+Total);
                                everyTen = true;
                            }
                        }.start();
                    }else {
                        if (firstTime){

                            Log.i("cobroMitad", "onChronometerTick: esperando los 10 gratis");
                        }else if (!everyTen){
                            Log.i("cobro10", "onChronometerTick: esperando los 10");
                        }
                    }
                }catch (Exception e){

                }



            }
        });
    }

    public void killAll(){
        pauseChronometer();
        timeParking.stop();
        progressBar.setVisibility(View.GONE);
        String fecha = null;
        Calendar calendar = Calendar.getInstance();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            fecha = format.format(calendar.getTime());
            textDateEnd.setText(getResources().getString(R.string.date_end)+" "+format.format(calendar.getTime()));
        }
        TinyDB tinyDB = new TinyDB(getApplicationContext());
        tinyDB.putString("fechaArriendo",fecha);
        tinyDB.putString("Time", timeParking.getText().toString());
        tinyDB.putString("PerHour","$ "+costoPH);
        tinyDB.putString("Total","$ "+Total);
        try {
            timer.cancel();
        }catch (Exception e){

        }

        timer = new CountDownTimer(6000, 1000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                DialogSelectedCard dialogNewCar = new DialogSelectedCard();
                dialogNewCar.setCancelable(false);
                dialogNewCar.show(getSupportFragmentManager(),"Tarjeta");



            }
        }.start();

    }

    private void startChronometer() {
        if (!running){
            timeParking.setBase(SystemClock.elapsedRealtime());
            timeParking.start();
            running = true;
        }
    }

    private void pauseChronometer() {
        if (running){
            timeParking.stop();
            pauseOffset = SystemClock.elapsedRealtime() - timeParking.getBase();
            running = false;
        }

    }

    private void init() {
        TinyDB tinyDB = new TinyDB(getApplicationContext());
        timeParking = findViewById(R.id.timeParking);
        timeParking.setVisibility(View.GONE);
        timeParking.setText("kk:mm:ss");


        btnStop = findViewById(R.id.btnStopParking);

        progressBar = findViewById(R.id.progressBar);

        TextView textDateStart = findViewById(R.id.textDateStart);
        textDateEnd = findViewById(R.id.textDateEnd);
        textCostEST = findViewById(R.id.textCostEstimate);
        textCostPH = findViewById(R.id.textCostParking);

        textPatente = findViewById(R.id.textPatente);

        costoPH = tinyDB.getInt("priceParking");
        costo10 = costoPH/6;
        Total = 0;
        firstTime = true;
        everyTen = false;

        textCostPH.setText(getResources().getString(R.string.price_p_h)+" "+costoPH);
        textPatente.setText(getResources().getString(R.string.licence)+" "+tinyDB.getString("patente"));

        Calendar calendar = Calendar.getInstance();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            textDateStart.setText(getResources().getString(R.string.date_end)+" "+format.format(calendar.getTime()));
        }



        //valor original / 6 = costo cada 10min
        //luego de los 30 despues de los 10 minutos de gracia

    }
}
