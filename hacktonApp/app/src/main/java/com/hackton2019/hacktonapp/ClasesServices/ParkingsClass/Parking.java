package com.hackton2019.hacktonapp.ClasesServices.ParkingsClass;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.hackton2019.hacktonapp.ConnectionServices.TinyDB;
import com.hackton2019.hacktonapp.ConnectionServices.Urls;
import com.hackton2019.hacktonapp.ConnectionServices.VolleySingleton;
import com.hackton2019.hacktonapp.Interfaces.CRUDbasic;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Parking{

    /**
     * ID PARKING
     */
    private String id;

    /**
     * USER ID PARKING
     */
    private String userID;

    /**
     * NAME PARKING
     */
    private String name;

    /**
     * GENERAL DESCRIPTION PARKING
     */
    private String description;

    /**
     * LATITTUDE PARKING
     */
    private double latittude;

    /**
     * LONGITUDE
     */
    private double longitude;

    /**
     * COST OF PARKING
     */
    private int cost;

    /**
     * STREEN NAME PARKING
     */
    private String street;

    /**
     * SECTOR NAME(COMUNA)
     */
    private String sector;

    /**
     * IF IS IN USE
     */
    boolean use;

    /**
     * IF IS VALID
     */
    boolean valid;

    /**
     * IF IS BLOCK
     */
    boolean lock;

    /**
     * REFERENCE IMAGE
     */
    String image;

    /**
     * RESPONSE CLASS STATE
     */
    boolean responseState;

    /**
     * STAR CALIFICATIONS
     */
    double stars;

    public Parking() {
    }

    public Parking(String id, String userID, String name, String description, double latittude, double longitude, int cost, String street, String sector, boolean use, boolean valid, boolean lock, String image, boolean responseState, double stars) {
        this.id = id;
        this.userID = userID;
        this.name = name;
        this.description = description;
        this.latittude = latittude;
        this.longitude = longitude;
        this.cost = cost;
        this.street = street;
        this.sector = sector;
        this.use = use;
        this.valid = valid;
        this.lock = lock;
        this.image = image;
        this.responseState = responseState;
        this.stars = stars;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLatittude() {
        return latittude;
    }

    public void setLatittude(double latittude) {
        this.latittude = latittude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public boolean isUse() {
        return use;
    }

    public void setUse(boolean use) {
        this.use = use;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public boolean isLock() {
        return lock;
    }

    public void setLock(boolean lock) {
        this.lock = lock;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isResponseState() {
        return responseState;
    }

    public void setResponseState(boolean responseState) {
        this.responseState = responseState;
    }

    public double getStars() {
        return stars;
    }

    public void setStars(double stars) {
        this.stars = stars;
    }


    public ArrayList<String> parkingDetail(final Context context){
        final ArrayList<String> parkingPhoto = new ArrayList<>();
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.MY_PARKING_DETAIL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject Obj ;

                        try {
                            Obj = new JSONObject(response);

                            if (!Obj.getBoolean("success")){
                                Toast.makeText(context,"Razon: "+Obj.getString("reason"),Toast.LENGTH_SHORT).show();
                            }else {
                                JSONArray imagenes = Obj.getJSONArray("urls");
                                for (int i = 0; i < imagenes.length(); i++) {
                                    parkingPhoto.add("https://pardob.pythonanywhere.com/"+imagenes.get(i));
                                    Log.i("url", "onResponse: "+parkingPhoto.get(i));
                                }
                                setName(Obj.getString("nombre"));

                                setStars(Obj.getDouble("reputacion"));

                                setDescription(Obj.getString("descripcion"));

                                setLock(Obj.getBoolean("lock"));

                                setCost(Obj.getInt("price"));

                                setUse(Obj.getBoolean("on_use"));

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"Error: "+error,Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", getId());
                params.put("is_mobile", "yes");
                Log.i("id", "getParams: "+getId());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                TinyDB tinyDB = new TinyDB(context);
                params.put("Authorization", "token "+tinyDB.getString("token"));
                Log.i("token user", "getHeaders: "+tinyDB.getString("token"));
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);



        try {

            JSONObject response = future.get(3, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Toast.makeText(context,"asd",Toast.LENGTH_SHORT).show();
        } catch (ExecutionException e) {
            Toast.makeText(context,"asd3",Toast.LENGTH_SHORT).show();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        return parkingPhoto;
    }

    public List<Parking> myParkings(final Context context){
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        final List<Parking> myParkings = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.MY_PARKING,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject Obj ;
                        Log.i("Cantidad", "onResponse: "+response);

                        try {
                            Obj = new JSONObject(response);

                            if (!Obj.getBoolean("success")){
                                Toast.makeText(context,Obj.getString("reason"),Toast.LENGTH_SHORT).show();
                                setResponseState(Obj.getBoolean("success"));
                            }else {
                                JSONArray listaEstacionamiento = Obj.getJSONArray("estacionamientos");
                                setResponseState(Obj.getBoolean("success"));
                                for (int i = 0; i < listaEstacionamiento.length(); i++) {
                                    Parking estacionamiento = new Parking();
                                    JSONObject estIndp = listaEstacionamiento.getJSONObject(i);
                                    estacionamiento.setId(estIndp.getString("id"));
                                    estacionamiento.setUserID(estIndp.getString("user"));
                                    estacionamiento.setName(estIndp.getString("name"));
                                    estacionamiento.setDescription(estIndp.getString("description"));
                                    estacionamiento.setLatittude(estIndp.getDouble("lat"));
                                    estacionamiento.setLongitude(estIndp.getDouble("lng"));
                                    estacionamiento.setCost(estIndp.getInt("user_defined_price"));
                                    estacionamiento.setStreet(estIndp.getString("calle"));
                                    estacionamiento.setSector(estIndp.getString("comuna"));
                                    estacionamiento.setUse(estIndp.getBoolean("on_use"));
                                    estacionamiento.setValid(estIndp.getBoolean("valid"));
                                    estacionamiento.setLock(estIndp.getBoolean("lock"));
                                    estacionamiento.setStars(estIndp.getDouble("calificacion"));

                                    myParkings.add(estacionamiento);
                                }
                            }

                            Log.i("Respuesta-Bien ", "onResponse: "+isResponseState());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"Error: "+error,Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                TinyDB tinyDB = new TinyDB(context);
                params.put("is_mobile", "yes");
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                TinyDB tinyDB = new TinyDB(context);
                params.put("Authorization", "token "+tinyDB.getString("token"));
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
        Log.i("Respuesta-Final ", "onResponse: "+isResponseState());


        try {

            JSONObject response = future.get(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Toast.makeText(context,"asd",Toast.LENGTH_SHORT).show();
        } catch (ExecutionException e) {
            Toast.makeText(context,"asd3",Toast.LENGTH_SHORT).show();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        return myParkings;
    }


    public boolean lockParking(final Context context, final String status){

        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.LOCK_PARKING,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject Obj ;

                        try {
                            Obj = new JSONObject(response);

                            if (!Obj.getBoolean("success")){
                                Toast.makeText(context,"Razon: "+Obj.getString("reason"),Toast.LENGTH_SHORT).show();
                                setResponseState(false);
                            }else {
                                setResponseState(true);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"Error: "+error,Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("idp", getId());
                params.put("op",status);
                Log.i("id", "getParams: "+getId());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                TinyDB tinyDB = new TinyDB(context);
                params.put("Authorization", "token "+tinyDB.getString("token"));
                Log.i("token user", "getHeaders: "+tinyDB.getString("token"));
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);


        try {

            JSONObject response = future.get(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Toast.makeText(context,"asd",Toast.LENGTH_SHORT).show();
        } catch (ExecutionException e) {
            Toast.makeText(context,"asd3",Toast.LENGTH_SHORT).show();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        return isResponseState();
    }

    public boolean startService(final Context context){

        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.START_SERVICE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject Obj ;

                        try {
                            Obj = new JSONObject(response);

                            if (!Obj.getBoolean("success")){
                                Toast.makeText(context,"Razon: "+Obj.getString("reason"),Toast.LENGTH_SHORT).show();
                                setResponseState(false);
                            }else {
                                setResponseState(true);
                                TinyDB tinyDB = new TinyDB(context);
                                tinyDB.putString("arriendoID",Obj.getString("arriendo"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"Error: "+error,Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                TinyDB tinyDB = new TinyDB(context);
                Log.i("que mando", "getParams: auto: "+tinyDB.getString("patenteID")+" est:"+tinyDB.getString("parkingID"));
                params.put("auto", tinyDB.getString("patenteID"));
                params.put("estacionamiento",tinyDB.getString("parkingID"));
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                TinyDB tinyDB = new TinyDB(context);
                params.put("Authorization", "token "+tinyDB.getString("token"));
                Log.i("token user", "getHeaders: "+tinyDB.getString("token"));
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);


        try {

            JSONObject response = future.get(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {

        } catch (ExecutionException e) {

        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        return isResponseState();
    }

    public boolean stopService(final Context context){

        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.STOP_SERVICE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject Obj ;

                        try {
                            Obj = new JSONObject(response);

                            if (!Obj.getBoolean("success")){
                                Toast.makeText(context,"Razon: "+Obj.getString("reason"),Toast.LENGTH_SHORT).show();
                                setResponseState(false);
                            }else {
                                setResponseState(true);
                                TinyDB tinyDB = new TinyDB(context);
                                tinyDB.putString("precio",Obj.getString("price"));

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"Error: "+error,Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                TinyDB tinyDB = new TinyDB(context);
                params.put("card", tinyDB.getString("cardID"));
                params.put("arriendo",  tinyDB.getString("arriendoID"));
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                TinyDB tinyDB = new TinyDB(context);
                params.put("Authorization", "token "+tinyDB.getString("token"));
                Log.i("token user", "getHeaders: "+tinyDB.getString("token"));
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);


        try {

            JSONObject response = future.get(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {

        } catch (ExecutionException e) {

        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        return isResponseState();
    }
}
