package com.hackton2019.hacktonapp.ClasesServices.UtilitiesClass;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.hackton2019.hacktonapp.ConnectionServices.TinyDB;
import com.hackton2019.hacktonapp.ConnectionServices.Urls;
import com.hackton2019.hacktonapp.ConnectionServices.VolleySingleton;
import com.hackton2019.hacktonapp.Interfaces.CRUDbasic;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class LicencePlate implements CRUDbasic {
    private String idPatente;

    private String patente;

    private String model;

    private String brand;

    private Context mContext;

    private List<LicencePlate> autosLista;

    public LicencePlate() {
    }

    public LicencePlate(String idPatente, String patente, String model, String brand, Context mContext, List<LicencePlate> autosLista) {
        this.idPatente = idPatente;
        this.patente = patente;
        this.model = model;
        this.brand = brand;
        this.mContext = mContext;
        this.autosLista = autosLista;
    }

    public String getIdPatente() {
        return idPatente;
    }

    public void setIdPatente(String idPatente) {
        this.idPatente = idPatente;
    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Context getmContext() {
        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    public List<LicencePlate> getAutosLista() {
        return autosLista;
    }

    public void setAutosLista(List<LicencePlate> autosLista) {
        this.autosLista = autosLista;
    }

    private boolean answer;
    @Override
    public boolean Read() {
            String url = "noLink";
            RequestFuture<JSONObject> future = RequestFuture.newFuture();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.CARS_WORKFLOW,
                    new Response.Listener<String    >() {
                        @Override
                        public void onResponse(String response) {
                            JSONObject Obj ;

                            Log.i("cars", "onResponse: "+response);

                            try {
                                Obj = new JSONObject(response);

                                if (!Obj.getBoolean("success")){
                                    Toast.makeText(mContext,"Razon: "+Obj.getString("reason"),Toast.LENGTH_SHORT).show();
                                    answer = false;

                                }else if (Obj.getBoolean("success")){
                                    List<LicencePlate> autosTemp = new ArrayList<>();
                                    JSONArray cars = Obj.getJSONArray("autos");
                                    for (int i = 0; i < cars.length(); i++) {
                                        JSONObject car = cars.getJSONObject(i);
                                        LicencePlate carTemp = new LicencePlate();
                                        carTemp.setPatente(car.getString("patente"));
                                        carTemp.setIdPatente(car.getString("id"));

                                        autosTemp.add(carTemp);
                                    }
                                    setAutosLista(autosTemp);
                                    answer = true;
                                }else {
                                    Log.i("wrong", "onResponse: Something went wrong at ReadCards");
                                    answer = false;
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(mContext,"Error: "+error,Toast.LENGTH_SHORT).show();
                    answer = false;
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("is_mobile","true");
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    TinyDB tinyDB = new TinyDB(mContext);
                    params.put("Authorization", "token "+tinyDB.getString("token"));
                    Log.i("token user", "getHeaders: "+tinyDB.getString("token"));
                    return params;
                }
            };
            VolleySingleton.getInstance(mContext).addToRequestQueue(stringRequest);


            try {

                JSONObject response = future.get(3, TimeUnit.SECONDS);
            } catch (InterruptedException e) {

            } catch (ExecutionException e) {

            } catch (TimeoutException e) {
                e.printStackTrace();
            }

      return answer;
    }

    @Override
    public boolean Del() {
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.CARS_WORKFLOW,
                new Response.Listener<String    >() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject Obj ;

                        Log.i("cars", "onResponse: "+response);

                        try {
                            Obj = new JSONObject(response);

                            if (!Obj.getBoolean("success")){
                                Toast.makeText(mContext,"Razon: "+Obj.getString("reason"),Toast.LENGTH_SHORT).show();
                                answer = false;

                            }else if (Obj.getBoolean("success")){
                                answer  = true;
                                Toast.makeText(mContext,"Eliminado exitosamente",Toast.LENGTH_SHORT).show();
                            }else {
                                Log.i("wrong", "onResponse: Something went wrong at ReadCards");
                                answer = false;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext,"Error: "+error,Toast.LENGTH_SHORT).show();
                answer = false;
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id",getIdPatente());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                TinyDB tinyDB = new TinyDB(mContext);
                params.put("Authorization", "token "+tinyDB.getString("token"));
                Log.i("token user", "getHeaders: "+tinyDB.getString("token"));
                return params;
            }
        };
        VolleySingleton.getInstance(mContext).addToRequestQueue(stringRequest);


        try {

            JSONObject response = future.get(3, TimeUnit.SECONDS);
        } catch (InterruptedException e) {

        } catch (ExecutionException e) {

        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        return answer;
    }

    @Override
    public boolean Create() {
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.CARS_WORKFLOW,
                new Response.Listener<String    >() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject Obj ;

                        Log.i("cars", "onResponse: "+response);

                        try {
                            Obj = new JSONObject(response);

                            if (!Obj.getBoolean("success")){
                                Toast.makeText(mContext,"Razon: "+Obj.getString("reason"),Toast.LENGTH_SHORT).show();
                                answer = false;

                            }else if (Obj.getBoolean("success")){
                                answer  = true;
                                Toast.makeText(mContext,"Añadido exitosamente",Toast.LENGTH_SHORT).show();
                            }else {
                                Log.i("wrong", "onResponse: Something went wrong at ReadCards");
                                answer = false;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext,"Error: "+error,Toast.LENGTH_SHORT).show();
                answer = false;
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                String patente = getPatente().substring(0,2)+" "+getPatente().substring(2,4)+" "+getPatente().substring(4,6);
                params.put("patente",patente);
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                TinyDB tinyDB = new TinyDB(mContext);
                params.put("Authorization", "token "+tinyDB.getString("token"));
                Log.i("token user", "getHeaders: "+tinyDB.getString("token"));
                return params;
            }
        };
        VolleySingleton.getInstance(mContext).addToRequestQueue(stringRequest);


        try {

            JSONObject response = future.get(3, TimeUnit.SECONDS);
        } catch (InterruptedException e) {

        } catch (ExecutionException e) {

        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        return answer;
    }

    @Override
    public boolean Updte() {
        return false;
    }
}
