package com.hackton2019.hacktonapp.ConnectionServices;

public class Urls {
        /**
         * ROOT URL WEBSERVICE
         */
        public static final String ROOT_URL = "https://pardob.pythonanywhere.com";
        //public static final String ROOT_URL = "http://192.168.43.199:8000";
        //public static final String ROOT_URL = "http://empresascab.cl/spatium";
        public static final String ADNROUD_URL = ROOT_URL+"/android";

        /**
         * DETAIL PARKING SELECTED URL
         */
        public static final String GET_USER_DETAIL = ROOT_URL+"/user";



        /**
         * LOCATE PARKING LIST URL
         */
        public static final String LOCATE_PARKINGS = ROOT_URL+"/getdataparking";

        /**
         * DETAIL PARKING SELECTED URL
         */
        public static final String MY_PARKING_DETAIL = ROOT_URL+"/parkingdetail";

        /**
         * PARKING LIST FOR OWNER PARKING
         */
        public static final String MY_PARKING = ROOT_URL+"/myparkings";

        /**
         * REGISTER STEP ONE TO CREATE USER
         */
        public static final String REGISTER_STEP_ONE = ROOT_URL+"/settel";

        /**
         * VALIDATE FIRST SMS
         */
        public static final String REGISTER_VALIDATE_SMS = ROOT_URL+"/comprobar";

        /**
         * CREATE USER
         */
        public static final String REGISTER_CREATE_USER = ROOT_URL+"/crear";

        /**
         * HIDE LOGIN
         */
        public static final String HIDE_LOGIN = ROOT_URL+"/api/login";

        /**
         * TO CONFIRM SMS
         */
        public static final String SMS_CONFIRM = ROOT_URL+"/smsconfirm";

        /**
         * TO RESEND SMS
         */
        public static final String RE_SEND_SMS = ROOT_URL+"/resend";

        /**
         * TO VALIDATE REAL EMAIL
         */
        public static final String EMAIL_VALIDATE= ROOT_URL+"/emailvalidate";


        /**
         * TO UNLOCK OR LOCK PARKING
         */
        public static final String LOCK_PARKING = ROOT_URL+"/lockparking";

        /**
         * GET COMMENTS
         */
        public static final String COMMENTS_STARS_PARKING = ROOT_URL+"/qualification";

        /**
         * SET COMMENTS
         */
        public static final String COMMENTS_STARS = ROOT_URL+"/qualify";

        /**
         * ADD NEW CARD
         */
        public static final String NEW_CREDIT_CARD = ROOT_URL+"/addcard";

        /**
         * CARDS
         */
        public static final String CARDS_WORKFLOW = ROOT_URL+"/credit";


        /**
         * CARS SERVICE
         */
        public static final String CARS_WORKFLOW = ROOT_URL+"/cars";

        /**
         * START SERVICE
         */
        public static final String START_SERVICE = ROOT_URL+"/startservice";

        /**
         * STOP SERVICE
         */
        public static final String STOP_SERVICE = ROOT_URL+"/stopservice";
    }
