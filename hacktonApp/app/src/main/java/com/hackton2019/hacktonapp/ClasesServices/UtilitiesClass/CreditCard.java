package com.hackton2019.hacktonapp.ClasesServices.UtilitiesClass;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.hackton2019.hacktonapp.ConnectionServices.TinyDB;
import com.hackton2019.hacktonapp.ConnectionServices.Urls;
import com.hackton2019.hacktonapp.ConnectionServices.VolleySingleton;
import com.hackton2019.hacktonapp.Interfaces.CRUDbasic;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class CreditCard  implements CRUDbasic {

    private String identificador;

    private String digitos;

    private String typeCard;

    private String url;

    private Context mContext;

    private List<CreditCard> listCards;

    public CreditCard() {
    }

    public CreditCard(String identificador, String digitos, String typeCard, String url, Context mContext, List<CreditCard> listCards) {
        this.identificador = identificador;
        this.digitos = digitos;
        this.typeCard = typeCard;
        this.url = url;
        this.mContext = mContext;
        this.listCards = listCards;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getDigitos() {
        return digitos;
    }

    public void setDigitos(String digitos) {
        this.digitos = digitos;
    }

    public String getTypeCard() {
        return typeCard;
    }

    public void setTypeCard(String typeCard) {
        this.typeCard = typeCard;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Context getmContext() {
        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    public List<CreditCard> getListCards() {
        return listCards;
    }

    public void setListCards(List<CreditCard> listCards) {
        this.listCards = listCards;
    }


    public String addNewCard(final Context context){

        String url = "noLink";
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.NEW_CREDIT_CARD,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject Obj ;
                        Log.i("urlCard", "onResponse: "+response);

                        try {
                            Obj = new JSONObject(response);
                            setUrl(Obj.getString("url"));
                            if (!Obj.getBoolean("success")){
                                Toast.makeText(context,"Razon: "+Obj.getString("reason"),Toast.LENGTH_SHORT).show();

                            }else {

                               setUrl(Obj.getString("url"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"Error: "+error,Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                TinyDB tinyDB = new TinyDB(context);
                Log.i("url", "getParams: "+Urls.ADNROUD_URL+"?hyj="+tinyDB.getString("hyj"));
                params.put("is_mobile","true");
                params.put("actualurl",Urls.ADNROUD_URL+"?hyj="+tinyDB.getString("hyj"));
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                TinyDB tinyDB = new TinyDB(context);
                params.put("Authorization", "token "+tinyDB.getString("token"));
                Log.i("token user", "getHeaders: "+tinyDB.getString("token"));
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);


        try {

            JSONObject response = future.get(3, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Toast.makeText(context,"asd",Toast.LENGTH_SHORT).show();
        } catch (ExecutionException e) {
            Toast.makeText(context,"asd3",Toast.LENGTH_SHORT).show();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        return url;
    }

    private boolean answer;
    @Override
    public boolean Read() {
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.CARDS_WORKFLOW,
                new Response.Listener<String    >() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject Obj ;

                        Log.i("cards", "onResponse: "+response);

                        try {
                            Obj = new JSONObject(response);

                            if (!Obj.getBoolean("success")){
                                Toast.makeText(mContext,"Razon: "+Obj.getString("reason"),Toast.LENGTH_SHORT).show();
                                answer = false;

                            }else if (Obj.getBoolean("success")){
                                List<CreditCard> tarjetasTemp = new ArrayList<>();
                                JSONArray cards = Obj.getJSONArray("tarjetas");
                                for (int i = 0; i < cards.length(); i++) {
                                    CreditCard creditCard = new CreditCard();
                                    JSONObject creditCardJson = cards.getJSONObject(i);

                                    creditCard.setIdentificador(creditCardJson.getString("identificador"));
                                    creditCard.setDigitos(creditCardJson.getString("ultimos_4_numeros"));
                                    creditCard.setTypeCard(creditCardJson.getString("tipo"));

                                    tarjetasTemp.add(creditCard);

                                }
                                setListCards(tarjetasTemp);
                                answer = true;
                            }else {
                                Log.i("wrong", "onResponse: Something went wrong at ReadCards");
                                answer = false;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext,"Error: "+error,Toast.LENGTH_SHORT).show();
                answer = false;
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("is_mobile","true");
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                TinyDB tinyDB = new TinyDB(mContext);
                params.put("Authorization", "token "+tinyDB.getString("token"));
                Log.i("token user", "getHeaders: "+tinyDB.getString("token"));
                return params;
            }
        };
        VolleySingleton.getInstance(mContext).addToRequestQueue(stringRequest);


        try {

            JSONObject response = future.get(3, TimeUnit.SECONDS);
        } catch (InterruptedException e) {

        } catch (ExecutionException e) {

        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        return answer;
    }

    @Override
    public boolean Del() {
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.CARDS_WORKFLOW,
                new Response.Listener<String    >() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject Obj ;

                        Log.i("cars", "onResponse: "+response);

                        try {
                            Obj = new JSONObject(response);

                            if (!Obj.getBoolean("success")){
                                Toast.makeText(mContext,"Razon: "+Obj.getString("reason"),Toast.LENGTH_SHORT).show();
                                answer = false;

                            }else if (Obj.getBoolean("success")){
                                answer  = true;
                                Toast.makeText(mContext,"Eliminado exitosamente",Toast.LENGTH_SHORT).show();
                            }else {
                                Log.i("wrong", "onResponse: Something went wrong at ReadCards");
                                answer = false;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext,"Error: "+error,Toast.LENGTH_SHORT).show();
                answer = false;
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id",getIdentificador());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                TinyDB tinyDB = new TinyDB(mContext);
                params.put("Authorization", "token "+tinyDB.getString("token"));
                Log.i("token user", "getHeaders: "+tinyDB.getString("token"));
                return params;
            }
        };
        VolleySingleton.getInstance(mContext).addToRequestQueue(stringRequest);


        try {

            JSONObject response = future.get(3, TimeUnit.SECONDS);
        } catch (InterruptedException e) {

        } catch (ExecutionException e) {

        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        return answer;
    }

    @Override
    public boolean Create() {
        return answer;
    }

    @Override
    public boolean Updte() {
        return answer;
    }
}
