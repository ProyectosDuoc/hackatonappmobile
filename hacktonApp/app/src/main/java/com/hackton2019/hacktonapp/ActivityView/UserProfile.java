package com.hackton2019.hacktonapp.ActivityView;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.hackton2019.hacktonapp.ClasesServices.UsersClass.User;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogWait;
import com.hackton2019.hacktonapp.HelperClass.Navigation;
import com.hackton2019.hacktonapp.R;

public class UserProfile extends AppCompatActivity implements View.OnClickListener {

    MaterialButton btnBack, btnUpdate;
    ImageView image;
    User user;

    TextInputEditText textName, textLast, textEmail, textNumber;
    DialogWait dialogWait;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile_activity);
        init();


    }

    private void init() {
        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);
        dialogWait = new DialogWait();
        user = new User();
        image = findViewById(R.id.profile_image);
        textEmail = findViewById(R.id.textCorreo);
        textName = findViewById(R.id.textNombre);
        textLast = findViewById(R.id.textApellido);
        textNumber = findViewById(R.id.textNumber);
        showDialg();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                user.userDetail(getApplicationContext());

                UserProfile.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Glide.with(getApplicationContext()).load(user.getImagenData()).into(image);
                        textEmail.setText(user.getEmail());
                        textLast.setText(user.getLastName());
                        textNumber.setText(user.getNumber());
                        textName.setText(user.getFirstName());
                        closeDialog();
                    }
                });
            }
        });
        thread.start();




    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnBack:
                Navigation.navegate(Navigation.MAP_ACTIVITY,Navigation.FLAG_FULL,getApplicationContext());
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Navigation.navegate(Navigation.MAP_ACTIVITY,Navigation.FLAG_FULL,getApplicationContext());
    }

    private void closeDialog() {
        dialogWait.dismiss();
    }

    private void showDialg() {
        dialogWait.setCancelable(false);
        dialogWait.show(getSupportFragmentManager(),"Cargando");
    }
}
