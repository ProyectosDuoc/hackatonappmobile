package com.hackton2019.hacktonapp.ActivityView.LoginUser;

import android.app.Activity;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.button.MaterialButton;
import com.hackton2019.hacktonapp.ActivityView.NewUser.FragmentNewProfile;
import com.hackton2019.hacktonapp.ClasesServices.UsersClass.User;
import com.hackton2019.hacktonapp.ConnectionServices.TinyDB;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogWait;
import com.hackton2019.hacktonapp.HelperClass.Navigation;
import com.hackton2019.hacktonapp.R;

public class FragmentSMSLogin extends Fragment {

    //**Activity Items
    View mView;
    Activity mMain;

    //**Fragment Items
    TextView textTimeLeft,textInfoPhone;
    EditText textSms;
    MaterialButton btnReSendSMS;

    User user;
    boolean answer;
    DialogWait dialogWait;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.new_login_user_sms,container,false);
        mMain = getActivity();
        init();
        conterTimeToReSend();

        btnReSendSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Thread threadB = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        TinyDB tinyDB = new TinyDB(getContext());
                        user.setNumber(tinyDB.getString("number"));
                        user.reSendSMS(getContext());

                        mMain.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btnReSendSMS.setEnabled(false);
                                conterTimeToReSend();
                            }
                        });
                    }
                });
                threadB.start();
            }
        });

        textSms.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                TinyDB tinyDB = new TinyDB(getContext());
                user.setNumber(tinyDB.getString("number"));
                user.setPasswordUser(tinyDB.getString("password"));
                user.setCodeValidation(textSms.getText().toString());

                if (editable.length()==4){
                    showDialg();
                    Thread threadA = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            user.registroLoginOculto(getActivity());
                            mMain.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    closeDialog();
                                    Navigation.navegate(Navigation.MAP_ACTIVITY,Navigation.FLAG_FULL,getContext());
                                    getActivity().finish();
                                }
                            });
                        }
                    });
                    threadA.start();

                }
            }
        });

        return mView;
    }

    private void conterTimeToReSend() {
        try{
            new CountDownTimer(30000, 1000) {
                public void onTick(long millisUntilFinished) {
                    textTimeLeft.setText(getResources().getString(R.string.iwait_time) +" "+ millisUntilFinished / 1000);
                }

                public void onFinish() {
                    textTimeLeft.setText(getResources().getString(R.string.info_wait_time));
                    btnReSendSMS.setEnabled(true);
                }
            }.start();
        }catch (Exception e){
            Log.i("excepcion", "conterTimeToReSend: "+e);
        }


    }

    private void init() {

        user = new User();
        dialogWait = new DialogWait();

        textInfoPhone = mView.findViewById(R.id.textInfoPhone);
        textTimeLeft = mView.findViewById(R.id.textTimeLeft);


        textSms = mView.findViewById(R.id.textSMS);


        btnReSendSMS = mView.findViewById(R.id.btnReSMS);

        chargeItemsInView();

    }

    private void chargeItemsInView() {
        TinyDB tinyDB = new TinyDB(getContext());
        textInfoPhone.setText(getResources().getString(R.string.info_phone)+" "+tinyDB.getString("number"));
    }


    private void closeDialog() {
        dialogWait.dismiss();
    }

    private void showDialg() {
        dialogWait.setCancelable(false);
        dialogWait.show(getActivity().getSupportFragmentManager(),"Cargando");
    }
}
