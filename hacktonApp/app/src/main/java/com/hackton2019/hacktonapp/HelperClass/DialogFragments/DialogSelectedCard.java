package com.hackton2019.hacktonapp.HelperClass.DialogFragments;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.hackton2019.hacktonapp.ActivityView.CreditCardActivity;
import com.hackton2019.hacktonapp.ActivityView.ServiceParking.GetCreditCardToPay;
import com.hackton2019.hacktonapp.ClasesServices.ParkingsClass.Parking;
import com.hackton2019.hacktonapp.ClasesServices.UtilitiesClass.CreditCard;
import com.hackton2019.hacktonapp.HelperClass.Adapters.CreditCardAdapter;
import com.hackton2019.hacktonapp.HelperClass.Navigation;
import com.hackton2019.hacktonapp.R;

import java.util.List;
import java.util.Objects;

public class DialogSelectedCard extends AppCompatDialogFragment {

    public RecyclerView mRecycleView;
    public GetCreditCardToPay mAdapter;
    public RecyclerView.LayoutManager mLayoutManager;

    List<CreditCard> creditCardList;
    CreditCard creditCard;

    MaterialButton btnNext;

    boolean answer;
    View view;
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        view = inflater.inflate(R.layout.dialog_card_list,null);

        btnNext = view.findViewById(R.id.btnNext);

        obtainCreditCards();

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Parking parking = new Parking();
                        parking.stopService(getContext());
                    }
                });
                thread.start();
                Navigation.navegate(Navigation.PAY_ACTIVITY,Navigation.FLAG_FULL,getContext());
            }
        });

        builder.setView(view);

        return builder.create();
    }

    private void builderList() {
        mRecycleView = view.findViewById(R.id.credit_card_recyclerView);
        mRecycleView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getContext());
        mAdapter = new GetCreditCardToPay(creditCardList,getContext(), getActivity());

        mRecycleView.setAdapter(mAdapter);
        mRecycleView.setLayoutManager(mLayoutManager);
    }

    public void obtainCreditCards() {

        Thread threadA = new Thread(new Runnable() {
            @Override
            public void run() {
                creditCard = new CreditCard();
                creditCard.setmContext(getContext());
                answer = creditCard.Read();
                Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (answer){
                            creditCardList = creditCard.getListCards();
                            builderList();
                        }else {
                            obtainCreditCards();
                        }
                    }
                });
            }
        });

        threadA.start();
    }
}
