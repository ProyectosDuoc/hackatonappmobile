package com.hackton2019.hacktonapp.HelperClass.DialogFragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.sliderviewlibrary.SliderView;
import com.google.android.gms.maps.model.LatLng;
import com.hackton2019.hacktonapp.ClasesServices.ParkingsClass.Parking;
import com.hackton2019.hacktonapp.ConnectionServices.TinyDB;
import com.hackton2019.hacktonapp.HelperClass.Navigation;
import com.hackton2019.hacktonapp.MainActivity;
import com.hackton2019.hacktonapp.R;
import com.location.aravind.getlocation.GeoLocator;

import java.util.ArrayList;

public class DialogParkingInfo extends AppCompatDialogFragment implements View.OnClickListener {

    /**
     * Buttons Dialog
     */
    Button btnHowCome,btnClose;

    /**
     * Text Info in Dialog
     */
    TextView costDialog,descriptionDialog,nameDialog,disponibility;

    /**
     * Galery To view images
     */
    SliderView galery;

    /**
     * Images url
     */
    ArrayList<String> images = new ArrayList<>();

    /**
     * Containter Items
     */

    RelativeLayout containerItem;

    /**
     * Container to wait
     */
    LinearLayout waitBar;

    /**
     * To find geolocalization
     */
    GeoLocator geoLocator;

    View view;

    Parking parking;

    //**Activity Items

    Activity mMain;
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        view = inflater.inflate(R.layout.dialog_info_parking,null);
        mMain = getActivity();
        init();
        infoParking();
        builder.setView(view);

        return builder.create();
    }

    private void infoParking() {
        TinyDB tinyDB = new TinyDB(getContext());
        parking = new Parking();
        parking.setId(tinyDB.getString("parkingID"));

        containerItem.setVisibility(View.GONE);
        waitBar.setVisibility(View.VISIBLE);

        Thread threadA = new Thread(new Runnable() {
            @Override
            public void run() {
                images = parking.parkingDetail(getContext());
                mMain.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        galery.setUrls(images);
                        costDialog.setText(getResources().getString(R.string.price)+" "+parking.getCost());
                        descriptionDialog.setText(getResources().getString(R.string.description)+" "+parking.getDescription());
                        nameDialog.setText(getResources().getString(R.string.name)+" "+parking.getName());
                        if (!parking.isUse()){
                            disponibility.setBackgroundResource(R.drawable.disponible_shape);
                            disponibility.setText(getResources().getString(R.string.Available));
                        }else if (parking.isUse()){
                            disponibility.setBackgroundResource(R.drawable.disponible_no_shape);
                            disponibility.setText(getResources().getString(R.string.Unavailable));
                        }
                        if (parking.isLock()){
                            disponibility.setBackgroundResource(R.drawable.disponible_lock_shape);
                            disponibility.setText(getResources().getString(R.string.lock_parking));
                        }
                        containerItem.setVisibility(View.VISIBLE);
                        waitBar.setVisibility(View.GONE);
                    }
                });
            }
        });

        threadA.start();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnHowCome:
                Navigation.navegate(Navigation.GLOBAL_INFO_ACTIVITY,Navigation.FLAG_FULL,getContext());
                getActivity().finish();
                //openGoogleMaps();
                break;
            case R.id.btnClose:
                getDialog().cancel();
                break;

        }
    }

    private void init() {

        btnHowCome = view.findViewById(R.id.btnHowCome);
        btnHowCome.setOnClickListener(this);
        btnClose = view.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(this);

        costDialog = view.findViewById(R.id.textCostParking);
        descriptionDialog = view.findViewById(R.id.textDescription);
        nameDialog = view.findViewById(R.id.textNameParking);
        galery = view.findViewById(R.id.galeryParking);
        disponibility = view.findViewById(R.id.dispibility);
        containerItem = view.findViewById(R.id.container_items_dialog);
        waitBar = view.findViewById(R.id.WaitBar);



        geoLocator = new GeoLocator(getContext(), getActivity());
        parking = new Parking();
    }

    private void openGoogleMaps() {
        TinyDB tinyDB = new TinyDB(getContext());
        LatLng miUbicacion = new LatLng(geoLocator.getLattitude(),geoLocator.getLongitude());
        LatLng miDestino = new LatLng(tinyDB.getDouble("markerLat",0.0),tinyDB.getDouble("markerLng",0.0));
        String uri = "http://maps.google.com/maps?saddr=" + miUbicacion.latitude + "," + miUbicacion.longitude + "(" + "Home Sweet Home" + ")&daddr=" + miDestino.latitude + "," + miDestino.longitude + " (" + "Where the party is at" + ")";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setPackage("com.google.android.apps.maps");
        startActivity(intent);
    }
}
