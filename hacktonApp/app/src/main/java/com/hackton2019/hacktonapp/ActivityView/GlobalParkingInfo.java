package com.hackton2019.hacktonapp.ActivityView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sliderviewlibrary.SliderView;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.button.MaterialButton;
import com.hackton2019.hacktonapp.ClasesServices.ParkingsClass.Parking;
import com.hackton2019.hacktonapp.ClasesServices.UtilitiesClass.Comments;
import com.hackton2019.hacktonapp.ClasesServices.UtilitiesClass.LicencePlate;
import com.hackton2019.hacktonapp.ConnectionServices.TinyDB;
import com.hackton2019.hacktonapp.HelperClass.Adapters.CommentsAdapter;
import com.hackton2019.hacktonapp.HelperClass.Adapters.MyCarAdapter;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogWait;
import com.hackton2019.hacktonapp.HelperClass.Navigation;
import com.hackton2019.hacktonapp.R;
import com.location.aravind.getlocation.GeoLocator;

import java.util.ArrayList;
import java.util.List;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class GlobalParkingInfo extends AppCompatActivity {

    /**
     * For getting comments
     */
    public RecyclerView mRecycleView;
    public CommentsAdapter mAdapter;
    public RecyclerView.LayoutManager mLayoutManager;
    List<Comments> comments;
    Comments commet;

    /**
     * For getting parking info
     */
    TinyDB tinyDB;
    ArrayList<String> imagesParking;
    Parking parking;

    /**
     * Configuration for parking info
     */
    SliderView galeryImages;
    TextView textNameParking,textReputation;
    MaterialRatingBar ratingBar;
    MaterialButton btnBack,btnLocate,btnService;

    /**
     * dialog to wait something
     */
    DialogWait dialogWait = new DialogWait();

    /**
     * To find geolocalization
     */
    GeoLocator geoLocator;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.global_parking_info);
        init();
        getParkingDetail();
        getParkingComments();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.navegate(Navigation.MAP_ACTIVITY,Navigation.FLAG_FULL,getApplicationContext());
                finish();
            }
        });

        btnLocate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGoogleMaps();
            }
        });

        btnService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.navegate(Navigation.BEFORE_ACTIVITY,Navigation.FLAG_FULL,getApplicationContext());
            }
        });
    }

    private void getParkingComments() {
        parking = new Parking();
        imagesParking = new ArrayList<>();
        parking.setId(tinyDB.getString("parkingID"));
        showDialg();
        Thread threadA = new Thread(new Runnable() {
            @Override
            public void run() {
                imagesParking = parking.parkingDetail(getApplicationContext());
                GlobalParkingInfo.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        galeryImages.setUrls(imagesParking);
                        textNameParking.setText(parking.getName());
                        textReputation.setText(String.valueOf(parking.getStars()/2));
                        ratingBar.setRating((float) (parking.getStars()/2));

                    }
                });
            }
        });
        threadA.start();
    }

    private void getParkingDetail() {
        commet = new Comments();
        commet.setIdParking(tinyDB.getString("parkingID"));
        Thread threadB = new Thread(new Runnable() {
            @Override
            public void run() {
                comments = commet.obtainComments(getApplicationContext());
                Log.i("comentarios", "run: "+comments.size());
                GlobalParkingInfo.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        builderComments();
                        closeDialog();
                    }
                });
            }
        });
        threadB.start();
    }

    private void init() {
        tinyDB = new TinyDB(getApplicationContext());
        mRecycleView = findViewById(R.id.global_comments_recyclerView);

        galeryImages = findViewById(R.id.global_galeryParking);
        textNameParking = findViewById(R.id.global_NameParking);
        textReputation = findViewById(R.id.global_textReputation);
        ratingBar = findViewById(R.id.global_ratingBar);
        btnBack = findViewById(R.id.btnBack);
        btnLocate = findViewById(R.id.btnLocate);
        btnService = findViewById(R.id.btnService);
    }


    private void builderComments() {
        mRecycleView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        Log.i("comentarios", "run: "+comments.size());
        mAdapter = new CommentsAdapter(comments,getApplicationContext(),GlobalParkingInfo.this);

        mRecycleView.setAdapter(mAdapter);
        mRecycleView.setLayoutManager(mLayoutManager);
    }


    private void closeDialog() {
        dialogWait.dismiss();
    }

    private void showDialg() {
        dialogWait.setCancelable(false);
        dialogWait.show(getSupportFragmentManager(),"Cargando");
    }


    private void openGoogleMaps() {
        geoLocator = new GeoLocator(getApplicationContext(),GlobalParkingInfo.this);
        TinyDB tinyDB = new TinyDB(getApplicationContext());
        LatLng miUbicacion = new LatLng(geoLocator.getLattitude(),geoLocator.getLongitude());
        LatLng miDestino = new LatLng(tinyDB.getDouble("markerLat",0.0),tinyDB.getDouble("markerLng",0.0));
        String uri = "http://maps.google.com/maps?saddr=" + miUbicacion.latitude + "," + miUbicacion.longitude + "(" + "Home Sweet Home" + ")&daddr=" + miDestino.latitude + "," + miDestino.longitude + " (" + "Where the party is at" + ")";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setPackage("com.google.android.apps.maps");
        startActivity(intent);
    }
}
