package com.hackton2019.hacktonapp.HelperClass.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.hackton2019.hacktonapp.ClasesServices.ParkingsClass.Parking;
import com.hackton2019.hacktonapp.ClasesServices.ParkingsClass.ParkingList;
import com.hackton2019.hacktonapp.ConnectionServices.TinyDB;
import com.hackton2019.hacktonapp.HelperClass.Navigation;
import com.hackton2019.hacktonapp.R;

import java.util.List;

public class SheetListAdapter extends RecyclerView.Adapter<SheetListAdapter.MyParking> {

    private List<Parking> myParkings;
    private Context mContext;
    private Activity mActivity;

    public SheetListAdapter() {
    }

    public SheetListAdapter(List<Parking> myParkings, Context mContext, Activity mActivity) {
        this.myParkings = myParkings;
        this.mContext = mContext;
        this.mActivity = mActivity;
    }

    public static class MyParking extends RecyclerView.ViewHolder{

        MaterialButton btnService;
        TextView nameParking;

        public MyParking(@NonNull View itemView) {
            super(itemView);
            btnService = itemView.findViewById(R.id.btnSheetParking);
            nameParking = itemView.findViewById(R.id.parking_sheet_text);
        }

        public void bindData(Parking parking, Context context, Activity mActivity){
            nameParking.setText(parking.getName());

        }
    }

    @NonNull
    @Override
    public MyParking onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sheet_item_view,parent,false);

        return new SheetListAdapter.MyParking(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyParking holder, int position) {
        holder.bindData(myParkings.get(position),mContext,mActivity);

        if (myParkings.get(holder.getAdapterPosition()).isUse()){
            holder.itemView.setVisibility(View.GONE);
        }

        holder.btnService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(mContext,"Hola", Toast.LENGTH_SHORT).show();
                TinyDB tinyDB = new TinyDB(mContext);
                tinyDB.putString("parkingID",myParkings.get(holder.getAdapterPosition()).getId());
                tinyDB.putInt("priceParking",myParkings.get(holder.getAdapterPosition()).getCost());
                Navigation.navegate(Navigation.BEFORE_ACTIVITY,Navigation.FLAG_FULL,mContext);
            }
        });
    }

    @Override
    public int getItemCount() {
        return myParkings.size();
    }
}
