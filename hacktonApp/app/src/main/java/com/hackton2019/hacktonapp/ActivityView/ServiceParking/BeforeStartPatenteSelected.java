package com.hackton2019.hacktonapp.ActivityView.ServiceParking;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.hackton2019.hacktonapp.ActivityView.MyCarActivity;
import com.hackton2019.hacktonapp.ClasesServices.UtilitiesClass.LicencePlate;
import com.hackton2019.hacktonapp.ConnectionServices.TinyDB;
import com.hackton2019.hacktonapp.HelperClass.Adapters.MyCarAdapter;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogWait;
import com.hackton2019.hacktonapp.R;

import java.util.List;

public class BeforeStartPatenteSelected extends RecyclerView.Adapter<BeforeStartPatenteSelected.MyHolder> {

    private List<LicencePlate> myLicences;
    private Context mContext;
    private BeforeStartConfig mActivity;

    private DialogWait dialogNWait;

    public BeforeStartPatenteSelected() {
    }

    public BeforeStartPatenteSelected(List<LicencePlate> myLicences, Context mContext, BeforeStartConfig mActivity) {
        this.myLicences = myLicences;
        this.mContext = mContext;
        this.mActivity = mActivity;
    }

    public static class MyHolder extends RecyclerView.ViewHolder{

        public TextView patente;
        public LinearLayout patenteSelected;
        public MyHolder(@NonNull View itemView) {
            super(itemView);
            patente = itemView.findViewById(R.id.textPatente);
            Typeface customFont = Typeface.createFromAsset(itemView.getContext().getAssets(), "fonts/FE.TTF");
            patente.setTypeface(customFont);
            patenteSelected = itemView.findViewById(R.id.patenteItem);


        }

        public void bindData(LicencePlate licence, Context context, Activity activity){
            patente.setText(licence.getPatente().toUpperCase());

        }
    }

    @NonNull
    @Override
    public BeforeStartPatenteSelected.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.before_patente_item,parent,false);


        return new BeforeStartPatenteSelected.MyHolder(view);
    }

    private boolean answer;
    private int mSelectedPosition = -1;
    private LinearLayout containerBtn;
    private TextView textoInfo;
    @Override
    public void onBindViewHolder(@NonNull final BeforeStartPatenteSelected.MyHolder holder, final int position) {
        holder.bindData(myLicences.get(position),mContext,mActivity);

        if(mSelectedPosition == position) {
            holder.patenteSelected.setBackgroundResource(R.drawable.patente_selected);
            TinyDB tinyDB = new TinyDB(mContext);
            tinyDB.putString("patenteID",myLicences.get(holder.getAdapterPosition()).getIdPatente());
            Log.i("patenteSelected", "onBindViewHolder: "+tinyDB.getString("patente"));
        }else{
            holder.patenteSelected.setBackgroundResource(R.drawable.patente);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textoInfo = mActivity.findViewById(R.id.informatorText);
                containerBtn = mActivity.findViewById(R.id.container_btns);

                textoInfo.setVisibility(View.VISIBLE);
                containerBtn.setVisibility(View.VISIBLE);

                mSelectedPosition = holder.getAdapterPosition();
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return myLicences.size();
    }


    private void closeDialog() {
        dialogNWait.dismiss();
    }

    private void showDialg() {
        dialogNWait = new DialogWait();
        dialogNWait.setCancelable(false);
        dialogNWait.show(((FragmentActivity)mActivity).getSupportFragmentManager(),"Espera");
    }


}
