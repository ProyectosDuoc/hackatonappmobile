package com.hackton2019.hacktonapp.ActivityView.NewUser;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.hackton2019.hacktonapp.ClasesServices.UsersClass.User;
import com.hackton2019.hacktonapp.ConnectionServices.TinyDB;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogWait;
import com.hackton2019.hacktonapp.HelperClass.Navigation;
import com.hackton2019.hacktonapp.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

public class FragmentNewProfile extends Fragment {
    private static final Pattern PASSWORD_PATTERN =
            Pattern.compile("^" +
                    //"(?=.*[0-9])" +         //at least 1 digit
                    //"(?=.*[a-z])" +         //at least 1 lower case letter
                    //"(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=.*[a-zA-Z])" +      //any letter
                    //"(?=.*[@#$%^&+=])" +    //at least 1 special character
                    "(?=\\S+$)" +           //no white spaces
                    ".{4,}" +               //at least 4 characters
                    "$");

    //**Activity Items
    private View mView;
    private Activity mMain;
    private static final int IMAGE_REQUEST = 1;

    //**Fragment Items
    private TextInputEditText textUsername;
    private TextInputEditText textEmail;
    private TextInputEditText textPassword;
    private MaterialButton btnNext;
    private ImageView photoProfile;
    private String currentImagePath;
    private User user;

    private DialogWait dialogWait;
    private Bitmap bitmap;
    boolean answer;
    private String imageData;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.new_user_profile,container,false);
        mMain = getActivity();
        init();


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (confirmInput()){
                    showDialg();
                    TinyDB tinyDB = new TinyDB(getContext());
                    user.setNumber(tinyDB.getString("number"));
                    user.setFirstName(tinyDB.getString("firstname"));
                    user.setLastName( tinyDB.getString("lastname"));
                    user.setUserName(textUsername.getText().toString());
                    user.setEmail(textEmail.getText().toString());
                    user.setPasswordUser(textPassword.getText().toString());
                    if (user.getImagenData()==null){
                        user.setImagenData("noImage");
                    }
                    Thread threadA = new Thread() {
                        public void run(){

                            answer = user.EmailCheck(getContext());

                            if (answer){
                                user.registroEtapaTresFinal(getContext());
                                mMain.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //dialog.hide();
                                        closeDialog();
                                        Navigation.navegate(Navigation.MAP_ACTIVITY,Navigation.FLAG_FULL,getContext());
                                        getActivity().finish();
                                        //mapIntentProces();
                                    }
                                });
                            }else {
                                mMain.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        textEmail.setError(mMain.getResources().getString(R.string.error_checked_email));
                                        Toast.makeText(getContext(),""+mMain.getResources().getString(R.string.error_checked_email),Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }


                        }
                    };
                    threadA.start();
                }


            }
        });

        photoProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePhoto();
            }
        });


        return mView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 0 && requestCode == IMAGE_REQUEST) {

        } else {
            Glide.with(getContext()).load(currentImagePath).into(photoProfile);
            bitmap = BitmapFactory.decodeFile(currentImagePath);
            imageData = imageToString(bitmap);

            //Log.i("imageData", "onActivityResult: " + imageData);

            user.setImagenData(imageData);
            //Log.i("imageData", "onActivityResult: " + user.getImagenData());
        }
    }

    private boolean validateEmail(){
        String emailInput = textEmail.getText().toString().trim();


        if (emailInput.isEmpty()){
            textEmail.setError("El campo no puede estar vacio");
            return false;
        }else if (!Patterns.EMAIL_ADDRESS.matcher(textEmail.getText()).matches()){
            textEmail.setError("Error al ingresar el correo");
            return false;
        }else {
            textEmail.setError(null);
            return true;
        }

    }

    private boolean validatePassword() {
        String passwordInput = textPassword.getText().toString().trim();

        if (passwordInput.isEmpty()) {
            textPassword.setError("Campo vacio");
            return false;
        } else if (!PASSWORD_PATTERN.matcher(passwordInput).matches()) {
            textPassword.setError("Contraseña muy debil");
            return false;
        } else {
            textPassword.setError(null);
            return true;
        }
    }

    public boolean confirmInput(){
        if (validateEmail() && validatePassword()){

            return true;
        }else {

        }
        Toast.makeText(getContext(), "Los datos ingresados estan incorrectos", Toast.LENGTH_SHORT).show();
        return false;

    }


    private void init() {
        user = new User();
        dialogWait = new DialogWait();
        textUsername = mView.findViewById(R.id.textUsername);
        textEmail = mView.findViewById(R.id.textMail);
        textPassword = mView.findViewById(R.id.textPass);

        photoProfile = mView.findViewById(R.id.profile_image);
        btnNext = mView.findViewById(R.id.btnNext);
    }


    private void takePhoto() {

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (cameraIntent.resolveActivity(mMain.getPackageManager()) != null) {
            File imageFile = null;

            try {
                imageFile = getImageFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (imageFile != null) {
                Uri imageUri = FileProvider.getUriForFile(getContext(), "com.example.android.spatium", imageFile);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                startActivityForResult(cameraIntent,IMAGE_REQUEST);

            }

        }



    }


    private String imageToString(Bitmap bitmap) {

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);
        byte[] imageBytes = outputStream.toByteArray();

        String encondedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encondedImage;
    }


    private File getImageFile() throws IOException {

        String imageName = "Photo_Perfil";
        File storagedir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File imageFile = File.createTempFile(imageName, ".jpg", storagedir);

        currentImagePath = imageFile.getAbsolutePath();

        return imageFile;

    }

    private void closeDialog() {
        dialogWait.dismiss();
    }

    private void showDialg() {
        dialogWait.setCancelable(false);
        dialogWait.show(getActivity().getSupportFragmentManager(),"Cargando");
    }

    private static Bitmap imagenCambio(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }
    private void getImage(){
        bitmap = BitmapFactory.decodeFile(currentImagePath);
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imageData, bounds);

        BitmapFactory.Options opts = new BitmapFactory.Options();
        Bitmap bm = BitmapFactory.decodeFile(imageData, opts);
        ExifInterface exif = null;
        File file = new File(imageData);
        try {
            exif = new ExifInterface(String.valueOf(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        int orientation = orientString != null ? Integer.parseInt(orientString) : ExifInterface.ORIENTATION_NORMAL;

        int rotationAngle = 0;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_180)
            rotationAngle = 180;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_270)
            rotationAngle = 270;

        imageData = imageToString(imagenCambio(bitmap, rotationAngle));

    }

}
