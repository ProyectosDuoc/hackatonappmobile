package com.hackton2019.hacktonapp.ActivityView;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.google.android.material.button.MaterialButton;
import com.hackton2019.hacktonapp.HelperClass.Navigation;
import com.hackton2019.hacktonapp.R;

public class AboutUs extends AppCompatActivity {

    MaterialButton btnBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_us);
        init();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.navegate(Navigation.MAP_ACTIVITY,Navigation.FLAG_FULL,getApplicationContext());
            }
        });
    }

    private void init() {
        btnBack = findViewById(R.id.btnBack);
    }

    @Override
    public void onBackPressed() {
        Navigation.navegate(Navigation.MAP_ACTIVITY,Navigation.FLAG_FULL,getApplicationContext());
    }
}
