package com.hackton2019.hacktonapp.HelperClass.Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.hackton2019.hacktonapp.ActivityView.MyCarActivity;
import com.hackton2019.hacktonapp.ClasesServices.UtilitiesClass.LicencePlate;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogNewCar;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogWait;
import com.hackton2019.hacktonapp.HelperClass.Navigation;
import com.hackton2019.hacktonapp.R;

import java.util.List;

public class MyCarAdapter extends RecyclerView.Adapter<MyCarAdapter.MyHolder> {

    private List<LicencePlate> myLicences;
    private Context mContext;
    private MyCarActivity mActivity;

    private DialogWait dialogNWait;

    public MyCarAdapter() {
    }

    public MyCarAdapter(List<LicencePlate> myLicences, Context mContext, MyCarActivity mActivity) {
        this.myLicences = myLicences;
        this.mContext = mContext;
        this.mActivity = mActivity;
    }

    public static class MyHolder extends RecyclerView.ViewHolder{

        TextView patente;
        FloatingActionButton btnDel;
        public MyHolder(@NonNull View itemView) {
            super(itemView);
            patente = itemView.findViewById(R.id.textPatente);
            Typeface customFont = Typeface.createFromAsset(itemView.getContext().getAssets(), "fonts/FE.TTF");
            patente.setTypeface(customFont);

            btnDel = itemView.findViewById(R.id.btnDel);
        }

        public void bindData(LicencePlate licence, Context context,Activity activity){

            patente.setText(licence.getPatente().toUpperCase());
        }
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_car_item,parent,false);


        return new MyCarAdapter.MyHolder(view);
    }

    private boolean answer;
    @Override
    public void onBindViewHolder(@NonNull final MyHolder holder, int position) {
        holder.bindData(myLicences.get(position),mContext,mActivity);

        holder.btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialg();
                Thread threadA = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        myLicences.get(holder.getAdapterPosition()).setmContext(mContext);
                        answer = myLicences.get(holder.getAdapterPosition()).Del();
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (answer){
                                    mActivity.obtainLicences();
                                    //Navigation.navegate(Navigation.CARS_ACTIVITY,Navigation.FLAG_FULL,mContext);
                                }else {
                                    Toast.makeText(mContext,"No se pudo eliminar",Toast.LENGTH_SHORT).show();
                                }
                                closeDialog();
                            }
                        });
                    }
                });

                threadA.start();



            }
        });
    }

    @Override
    public int getItemCount() {
        return myLicences.size();
    }


    private void closeDialog() {
        dialogNWait.dismiss();
    }

    private void showDialg() {
        dialogNWait = new DialogWait();
        dialogNWait.setCancelable(false);
        dialogNWait.show(((FragmentActivity)mActivity).getSupportFragmentManager(),"Espera");
    }


}
