package com.hackton2019.hacktonapp.HelperClass.DialogFragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;


import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.hackton2019.hacktonapp.ClasesServices.UtilitiesClass.Comments;
import com.hackton2019.hacktonapp.HelperClass.Navigation;
import com.hackton2019.hacktonapp.R;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;


public class DialogComment extends AppCompatDialogFragment {

    private TextInputEditText comentario;
    private MaterialButton btnCancel;
    private MaterialButton btnSend;
    private MaterialRatingBar ratingbar;

    Comments comments;
    private float ratingUser;
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_comment, null);

        comentario = view.findViewById(R.id.textComment);
        btnCancel = view.findViewById(R.id.btnCancel);
        btnSend = view.findViewById(R.id.btnsend);
        ratingbar = view.findViewById(R.id.calification_ratingBar_item);

        ratingbar.setOnRatingChangeListener(new MaterialRatingBar.OnRatingChangeListener() {
            @Override
            public void onRatingChanged(MaterialRatingBar ratingBar, float rating) {
                ratingUser = rating;
                Log.i("rating", "onRatingChanged: "+rating);
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comments = new Comments();
                comments.setCommentUser(comentario.getText().toString());
                comments.setNota((int) ratingUser);
                Thread threadA = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        comments.sendComment(getContext());
                        Navigation.navegate(Navigation.MAP_ACTIVITY,Navigation.FLAG_FULL,getContext());

                    }
                });
                threadA.start();


            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.navegate(Navigation.MAP_ACTIVITY,Navigation.FLAG_FULL,getContext());
            }
        });


        builder.setView(view);

        return builder.create();
    }
}
