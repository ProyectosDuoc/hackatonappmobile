package com.hackton2019.hacktonapp.HelperClass.DialogFragments;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.hackton2019.hacktonapp.ClasesServices.ParkingsClass.Parking;
import com.hackton2019.hacktonapp.ClasesServices.ParkingsClass.ParkingList;
import com.hackton2019.hacktonapp.HelperClass.Adapters.ParkingListAdapter;
import com.hackton2019.hacktonapp.HelperClass.Adapters.SheetListAdapter;
import com.hackton2019.hacktonapp.MainActivity;
import com.hackton2019.hacktonapp.R;

import java.util.ArrayList;
import java.util.List;

public class BottomSheetListParking extends BottomSheetDialogFragment {

    private RecyclerView mRecycleView;
    private SheetListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public Activity mActivity;

    private LinearLayout espera,noRespuesta;
    private boolean answer;
    private ParkingList parkingList = new ParkingList();
    private TextView textoEspera;
    List<Parking> myList;
    public static BottomSheetListParking newInstance() {
        BottomSheetListParking fragment = new BottomSheetListParking();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        init();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void init() {

        setmRecycleView();

    }

    private void builderList() {


        mRecycleView.setAdapter(mAdapter);
        mRecycleView.setLayoutManager(mLayoutManager);
    }

    private void setmRecycleView() {
        Log.i("que es", "cargaContenedor: "+getChildFragmentManager());
        mRecycleView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getContext());
        espera.setVisibility(View.VISIBLE);
        Thread threadA = new Thread(new Runnable() {
            @Override
            public void run() {

               answer = parkingList.fetchList(getContext(),MainActivity.geoPositionCaptured);
                Log.i("lengthSheet", "run: "+parkingList.getParkings());
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (answer && parkingList.getParkings().size()>=1){
                            myList = new ArrayList<>();
                            for (int i = 0; i < parkingList.getParkings().size(); i++) {
                                if (!parkingList.getParkings().get(i).isUse()){
                                    myList.add(parkingList.getParkings().get(i));

                                }
                            }
                            mAdapter = new SheetListAdapter(myList,getContext(),getActivity());
                            builderList();
                            espera.setVisibility(View.GONE);
                        }else {
                            noRespuesta.setVisibility(View.VISIBLE);
                            espera.setVisibility(View.GONE);

                             new CountDownTimer(10000, 1000) {
                                @Override
                                public void onTick(long l) {
                                    textoEspera.setText(mActivity.getResources().getString(R.string.asking_again)+" "+l/1000);
                                }

                                @Override
                                public void onFinish() {
                                    setmRecycleView();
                                    noRespuesta.setVisibility(View.GONE);
                                    espera.setVisibility(View.VISIBLE);

                                }
                            }.start();
                        }

                    }
                });
            }
        });
        threadA.start();




    }

    @Override
    public void setupDialog(@NonNull Dialog dialog, int style) {
        View contentView = View.inflate(getContext(), R.layout.sheet_main, null);
        dialog.setContentView(contentView);

        mRecycleView = dialog.findViewById(R.id.sheet_recyclerView);
        espera = dialog.findViewById(R.id.espera);
        noRespuesta = dialog.findViewById(R.id.noHay);
        textoEspera = dialog.findViewById(R.id.textoEspera);

    }
}
