package com.hackton2019.hacktonapp.ClasesServices.UtilitiesClass;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.hackton2019.hacktonapp.ConnectionServices.TinyDB;
import com.hackton2019.hacktonapp.ConnectionServices.Urls;
import com.hackton2019.hacktonapp.ConnectionServices.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Comments {

    /**
     * Comments ID
     */
    String idComent;

    /**
     * First Name + Last Name
     */
    String user;

    /**
     * User image
     */
    String userImage;

    /**
     * User Calification
     */
    int nota;

    /**
     * User comment
     */
    String commentUser;

    /**
     * Date of comments;
     */
    String dateComment;

    /**
     * Parking ID
     */
    String idParking;


    public Comments() {
    }

    public Comments(String idComent, String user, String userImage, int nota, String commentUser, String dateComment, String idParking) {
        this.idComent = idComent;
        this.user = user;
        this.userImage = userImage;
        this.nota = nota;
        this.commentUser = commentUser;
        this.dateComment = dateComment;
        this.idParking = idParking;
    }

    public String getIdComent() {
        return idComent;
    }

    public void setIdComent(String idComent) {
        this.idComent = idComent;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }

    public String getCommentUser() {
        return commentUser;
    }

    public void setCommentUser(String commentUser) {
        this.commentUser = commentUser;
    }

    public String getDateComment() {
        return dateComment;
    }

    public void setDateComment(String dateComment) {
        this.dateComment = dateComment;
    }

    public String getIdParking() {
        return idParking;
    }

    public void setIdParking(String idParking) {
        this.idParking = idParking;
    }

    public List<Comments> obtainComments(final Context context){
        final List<Comments> comentarios = new ArrayList<>();
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.COMMENTS_STARS_PARKING,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject Obj ;

                        try {
                            Obj = new JSONObject(response);
                            Log.i("comentarios", "onResponse: "+response);
                            if (!Obj.getBoolean("success")){
                                Toast.makeText(context,"Razon: "+Obj.getString("reason"),Toast.LENGTH_SHORT).show();
                            }else {
                                JSONArray ComentariosEst = Obj.getJSONArray("calificaciones");
                                for (int i = 0; i < ComentariosEst.length(); i++) {
                                    JSONObject comentEst = ComentariosEst.getJSONObject(i);
                                    Comments comentario = new Comments();
                                    comentario.setUser(comentEst.getString("user"));
                                    comentario.setUserImage("https://pardob.pythonanywhere.com/"+comentEst.getString("userimage"));
                                    comentario.setNota(comentEst.getInt("nota"));
                                    comentario.setDateComment(comentEst.getString("fecha"));
                                    comentario.setCommentUser(comentEst.getString("comentario"));
                                    comentarios.add(comentario);
                                    Log.i("comentarios", "onResponse: "+comentarios.size());

                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"Error: "+error,Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", getIdParking());
                params.put("is_mobile", "yes");
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                TinyDB tinyDB = new TinyDB(context);
                params.put("Authorization", "token "+tinyDB.getString("token"));
                Log.i("token user", "getHeaders: "+tinyDB.getString("token"));
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);


        try {

            JSONObject response = future.get(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Toast.makeText(context,"asd",Toast.LENGTH_SHORT).show();
        } catch (ExecutionException e) {
            Toast.makeText(context,"asd3",Toast.LENGTH_SHORT).show();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        return comentarios;
    }

    public void sendComment(final Context context){
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.COMMENTS_STARS,
                new Response.Listener<String    >() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("cars", "onResponse: "+response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                TinyDB tinyDB = new TinyDB(context);
                params.put("id",tinyDB.getString("parkingID"));
                params.put("nota",String.valueOf(getNota()*2));
                params.put("comentario",getCommentUser());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                TinyDB tinyDB = new TinyDB(context);
                params.put("Authorization", "token "+tinyDB.getString("token"));
                Log.i("token user", "getHeaders: "+tinyDB.getString("token"));
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);


        try {

            JSONObject response = future.get(3, TimeUnit.SECONDS);
        } catch (InterruptedException e) {

        } catch (ExecutionException e) {

        } catch (TimeoutException e) {
            e.printStackTrace();
        }


    }
}
