package com.hackton2019.hacktonapp.ClasesServices.ParkingsClass;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.hackton2019.hacktonapp.ConnectionServices.Urls;
import com.hackton2019.hacktonapp.ConnectionServices.VolleySingleton;
import com.here.android.mpa.common.GeoPosition;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.security.auth.login.LoginException;

public class ParkingList {

    private List<Parking> Parkings;
    private List<Parking> ParkingsLock;
    private List<Parking> ParkingsFar;

    public ParkingList() {
    }

    public ParkingList(List<Parking> parkings, List<Parking> parkingsLock, List<Parking> parkingsFar) {
        Parkings = parkings;
        ParkingsLock = parkingsLock;
        ParkingsFar = parkingsFar;
    }

    public List<Parking> getParkings() {
        return Parkings;
    }

    public void setParkings(List<Parking> parkings) {
        Parkings = parkings;
    }

    public List<Parking> getParkingsLock() {
        return ParkingsLock;
    }

    public void setParkingsLock(List<Parking> parkingsLock) {
        ParkingsLock = parkingsLock;
    }

    public List<Parking> getParkingsFar() {
        return ParkingsFar;
    }

    public void setParkingsFar(List<Parking> parkingsFar) {
        ParkingsFar = parkingsFar;
    }

    private boolean answer=false;
    public boolean fetchList(final Context mContext, final GeoPosition position){

        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.LOCATE_PARKINGS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            JSONArray closeParking,farParking,lockParking;

                            if (responseObject.getBoolean("success")){
                                answer = responseObject.getBoolean("success");
                                closeParking = responseObject.getJSONArray("result");
                                farParking = responseObject.getJSONArray("far");
                                lockParking = responseObject.getJSONArray("locked");
                                List<Parking> parkings = new ArrayList<>();
                                List<Parking> parkingsFar = new ArrayList<>();
                                List<Parking> parkingsLock = new ArrayList<>();
                                Log.i("CloseParking", "onResponse: "+closeParking.length());
                                for (int i = 0; i < closeParking.length(); i++) {
                                    JSONObject closePark = closeParking.getJSONObject(i);

                                    Parking parking = new Parking();
                                    parking.setId(closePark.getString("id"));
                                    parking.setUserID(closePark.getString("user"));
                                    parking.setName(closePark.getString("name"));
                                    parking.setDescription(closePark.getString("description"));
                                    parking.setLatittude(closePark.getDouble("lat"));
                                    parking.setLongitude(closePark.getDouble("lng"));
                                    parking.setCost(closePark.getInt("user_defined_price"));
                                    parking.setStreet(closePark.getString("calle"));
                                    parking.setSector(closePark.getString("comuna"));
                                    parking.setUse(closePark.getBoolean("on_use"));
                                    parking.setValid(closePark.getBoolean("valid"));
                                    parking.setLock(closePark.getBoolean("lock"));

                                    parkings.add(parking);

                                }


                                for (int i = 0; i < farParking.length(); i++) {
                                    JSONObject closePark = farParking.getJSONObject(i);

                                    Parking parking = new Parking();
                                    parking.setId(closePark.getString("id"));
                                    parking.setUserID(closePark.getString("user"));
                                    parking.setName(closePark.getString("name"));
                                    parking.setDescription(closePark.getString("description"));
                                    parking.setLatittude(closePark.getDouble("lat"));
                                    parking.setLongitude(closePark.getDouble("lng"));
                                    parking.setCost(closePark.getInt("user_defined_price"));
                                    parking.setStreet(closePark.getString("calle"));
                                    parking.setSector(closePark.getString("comuna"));
                                    parking.setUse(closePark.getBoolean("on_use"));
                                    parking.setValid(closePark.getBoolean("valid"));
                                    parking.setLock(closePark.getBoolean("lock"));

                                    parkingsFar.add(parking);

                                }


                                for (int i = 0; i < lockParking.length(); i++) {
                                    JSONObject closePark = lockParking.getJSONObject(i);

                                    Parking parking = new Parking();
                                    parking.setId(closePark.getString("id"));
                                    parking.setUserID(closePark.getString("user"));
                                    parking.setName(closePark.getString("name"));
                                    parking.setDescription(closePark.getString("description"));
                                    parking.setLatittude(closePark.getDouble("lat"));
                                    parking.setLongitude(closePark.getDouble("lng"));
                                    parking.setCost(closePark.getInt("user_defined_price"));
                                    parking.setStreet(closePark.getString("calle"));
                                    parking.setSector(closePark.getString("comuna"));
                                    parking.setUse(closePark.getBoolean("on_use"));
                                    parking.setValid(closePark.getBoolean("valid"));
                                    parking.setLock(closePark.getBoolean("lock"));

                                    parkingsLock.add(parking);

                                }
                                setParkings(parkings);
                                setParkingsFar(parkingsFar);
                                setParkingsLock(parkingsLock);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.i("ListParking", "onResponse: "+response);


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext,"Error: "+error,Toast.LENGTH_SHORT).show();
                Log.i("error", "onErrorResponse: "+error);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("lat", String.valueOf(position.getCoordinate().getLatitude()));
                params.put("lng", String.valueOf(position.getCoordinate().getLongitude()));
                return params;
            }
        };
        VolleySingleton.getInstance(mContext).addToRequestQueue(stringRequest);


        try {

            JSONObject response = future.get(2, TimeUnit.SECONDS);
        } catch (InterruptedException e) {

        } catch (ExecutionException e) {

        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        return answer;
    }

}
