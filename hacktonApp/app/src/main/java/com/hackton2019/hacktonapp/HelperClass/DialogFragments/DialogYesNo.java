package com.hackton2019.hacktonapp.HelperClass.DialogFragments;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.google.android.material.button.MaterialButton;
import com.hackton2019.hacktonapp.ActivityView.ServiceParking.parkimetroActivity;
import com.hackton2019.hacktonapp.ClasesServices.ParkingsClass.Parking;
import com.hackton2019.hacktonapp.HelperClass.Navigation;
import com.hackton2019.hacktonapp.R;

import java.util.Objects;

public class DialogYesNo extends AppCompatDialogFragment {

    public parkimetroActivity parkimetro;
    MaterialButton btnYes,btnNo;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_yes_no,null);

        btnYes = view.findViewById(R.id.btnYes);
        btnNo = view.findViewById(R.id.btnNo);

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (parkimetro == null){

                    Parking parking = new Parking();
                    parking.startService(getContext());

                    Navigation.navegate(Navigation.PARKIMETRO_ACTIVITY,Navigation.FLAG_FULL,getContext());
                }else {
                    parkimetro.killAll();

                    Objects.requireNonNull(getDialog()).dismiss();
                }

            }
        });
        builder.setView(view);

        return builder.create();
    }
}
