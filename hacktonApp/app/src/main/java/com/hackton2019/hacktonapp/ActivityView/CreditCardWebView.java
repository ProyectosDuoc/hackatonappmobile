package com.hackton2019.hacktonapp.ActivityView;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;


import com.hackton2019.hacktonapp.ClasesServices.UtilitiesClass.CreditCard;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.javaScripHelper;
import com.hackton2019.hacktonapp.HelperClass.Navigation;

import com.hackton2019.hacktonapp.R;

public class CreditCardWebView extends AppCompatActivity {

    WebView webView;
    CreditCard creditCard;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.credit_card_web_view);

        newCard();
        init();
    }

    private void newCard() {
        creditCard = new CreditCard();
        webView = new WebView(getApplicationContext());
        Thread threadA = new Thread(new Runnable() {
            @Override
            public void run() {

                creditCard.addNewCard(getApplicationContext());
                CreditCardWebView.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                        webView.loadUrl(creditCard.getUrl());
                        setContentView(webView);
                    }
                });
            }
        });

        threadA.start();
    }

    @Override
    public void onBackPressed() {
        Navigation.navegate(Navigation.PAYMENT_ACTIVITY,Navigation.FLAG_FULL,getApplicationContext());
    }


    private void init() {



        webView.clearFormData();
        webView.clearCache(true);


        webView.getSettings().setJavaScriptEnabled(true);





        webView.addJavascriptInterface(new WebAppInterface(this), "Android");




    }


    public class WebAppInterface {
        Context mContext;

        /** Instantiate the interface and set the context */
        WebAppInterface(Context c) {
            mContext = c;
        }

        /** Show a toast from the web page */
        @JavascriptInterface
        public void showToast(String toast) {
            Navigation.navegate(Navigation.PAYMENT_ACTIVITY,Navigation.FLAG_FULL,mContext);
        }
    }
}
