package com.hackton2019.hacktonapp.ActivityView.ServiceParking;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.hackton2019.hacktonapp.ActivityView.CreditCardActivity;
import com.hackton2019.hacktonapp.ClasesServices.UtilitiesClass.CreditCard;
import com.hackton2019.hacktonapp.ConnectionServices.TinyDB;
import com.hackton2019.hacktonapp.HelperClass.Adapters.CreditCardAdapter;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogWait;
import com.hackton2019.hacktonapp.R;

import java.util.List;

public class GetCreditCardToPay extends RecyclerView.Adapter<GetCreditCardToPay.MyHolder> {

    private List<CreditCard> creditCards;
    private Context mContext;
    private Activity mActivity;

    private DialogWait dialogWait;

    public GetCreditCardToPay() {
    }

    public GetCreditCardToPay(List<CreditCard> creditCards, Context mContext, Activity mActivity) {
        this.creditCards = creditCards;
        this.mContext = mContext;
        this.mActivity = mActivity;
    }

    public static class MyHolder extends RecyclerView.ViewHolder {

        TextView lastDigits;
        ImageView typeCardImage;
        FloatingActionButton btnDel;
        LinearLayout cardForm;
        public MyHolder(@NonNull View itemView) {
            super(itemView);
            lastDigits = itemView.findViewById(R.id.lastFourDigits);
            typeCardImage = itemView.findViewById(R.id.credit_card_image);
            btnDel = itemView.findViewById(R.id.btnDel);
            cardForm = itemView.findViewById(R.id.cardForm);
            btnDel.setVisibility(View.GONE);
        }

        public void bindData(CreditCard card, Context context, Activity activity) {
            lastDigits.setText(" " + card.getDigitos());
            if (card.getTypeCard().equals("Visa")) {
                typeCardImage.setImageResource(R.drawable.visa);

            }
            if (card.getTypeCard().equals("Mastercard")) {
                typeCardImage.setImageResource(R.drawable.master);
            }

        }
    }

    @NonNull
    @Override
    public GetCreditCardToPay.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.credit_card_item, parent, false);


        return new GetCreditCardToPay.MyHolder(view);
    }

    private int mSelectedPosition = -1;
    @Override
    public void onBindViewHolder(@NonNull final GetCreditCardToPay.MyHolder holder, int position) {
        holder.bindData(creditCards.get(position), mContext, mActivity);
        if(mSelectedPosition == position) {
            holder.cardForm.setBackgroundResource(R.drawable.shape_credit_card_selected);
            TinyDB tinyDB = new TinyDB(mContext);
            tinyDB.putString("cardID",creditCards.get(holder.getAdapterPosition()).getIdentificador());
            Log.i("cardSelected", "onBindViewHolder: "+tinyDB.getString("cardID"));
        }else{
            holder.cardForm.setBackgroundResource(R.drawable.shape_credit_card);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mSelectedPosition = holder.getAdapterPosition();
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return creditCards.size();
    }

    private void closeDialog() {
        dialogWait.dismiss();
    }

    private void showDialg() {
        dialogWait = new DialogWait();
        dialogWait.setCancelable(false);
        dialogWait.show(((FragmentActivity) mActivity).getSupportFragmentManager(), "Espera");
    }
}