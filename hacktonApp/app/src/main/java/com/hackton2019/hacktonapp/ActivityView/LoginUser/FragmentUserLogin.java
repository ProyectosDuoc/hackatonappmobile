package com.hackton2019.hacktonapp.ActivityView.LoginUser;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.hackton2019.hacktonapp.ClasesServices.UsersClass.User;
import com.hackton2019.hacktonapp.ConnectionServices.TinyDB;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogWait;
import com.hackton2019.hacktonapp.R;

public class FragmentUserLogin extends Fragment {

    //**Activity Items
    View mView;
    Activity mMain;

    //**Fragment Items
    TextInputEditText textNumberPhone,textPassword;
    MaterialButton btnNext;

    FragmentSMSLogin smsLogin;
    User user;
    boolean answer;
    DialogWait dialogWait;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.login_user_fragment,container,false);
        mMain = getActivity();
        init();


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialg();
                user.setNumber(textNumberPhone.getText().toString());
                user.setPasswordUser(textPassword.getText().toString());
                TinyDB tinyDB = new TinyDB(getContext());
                tinyDB.putString("number",user.getNumber());
                tinyDB.putString("password",user.getPasswordUser());
                Thread threadA = new Thread(new Runnable() {
                    @Override
                    public void run() {
                     answer =  user.SMSConfirm(getContext());
                        mMain.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (answer){
                                    closeDialog();
                                    FragmentTransaction fmt = getActivity().getSupportFragmentManager().beginTransaction();
                                    fmt.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                                    fmt.replace(R.id.containerItems,smsLogin);
                                    fmt.commit();
                                }else {
                                    closeDialog();
                                    Toast.makeText(getContext(),"Su cuenta o contraseña estan incorrectas",Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                });
                threadA.start();

            }
        });


        return mView;
    }

    private void init() {
        smsLogin = new FragmentSMSLogin();
        user = new User();
        dialogWait = new DialogWait();

        textNumberPhone = mView.findViewById(R.id.textNumerPhone);
        textPassword = mView.findViewById(R.id.textPass);

        btnNext = mView.findViewById(R.id.btnNext);
    }

    private void closeDialog() {
        dialogWait.dismiss();
    }

    private void showDialg() {
        dialogWait.setCancelable(false);
        dialogWait.show(getActivity().getSupportFragmentManager(),"Cargando");
    }
}
