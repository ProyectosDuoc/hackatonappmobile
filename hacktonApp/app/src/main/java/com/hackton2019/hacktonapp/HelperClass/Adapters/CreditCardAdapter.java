package com.hackton2019.hacktonapp.HelperClass.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.hackton2019.hacktonapp.ActivityView.CreditCardActivity;
import com.hackton2019.hacktonapp.ClasesServices.UtilitiesClass.CreditCard;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogWait;
import com.hackton2019.hacktonapp.R;

import java.util.List;

public class CreditCardAdapter extends RecyclerView.Adapter<CreditCardAdapter.MyHolder> {

    private List<CreditCard> creditCards;
    private Context mContext;
    private CreditCardActivity mActivity;

    private DialogWait dialogWait;

    public CreditCardAdapter() {
    }

    public CreditCardAdapter(List<CreditCard> creditCards, Context mContext, CreditCardActivity mActivity) {
        this.creditCards = creditCards;
        this.mContext = mContext;
        this.mActivity = mActivity;
    }

    public static class MyHolder extends RecyclerView.ViewHolder{

        TextView lastDigits;
        ImageView typeCardImage;
        FloatingActionButton btnDel;
        public MyHolder(@NonNull View itemView) {
            super(itemView);
            lastDigits = itemView.findViewById(R.id.lastFourDigits);
            typeCardImage = itemView.findViewById(R.id.credit_card_image);
            btnDel = itemView.findViewById(R.id.btnDel);
        }

        public void bindData(CreditCard card, Context context, Activity activity) {
            lastDigits.setText(" "+card.getDigitos());
            if (card.getTypeCard().equals("Visa")){
                typeCardImage.setImageResource(R.drawable.visa);

            }if (card.getTypeCard().equals("Mastercard")){
                typeCardImage.setImageResource(R.drawable.master);
            }

        }
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.credit_card_item,parent,false);


        return new CreditCardAdapter.MyHolder(view);
    }
    private boolean answer;
    @Override
    public void onBindViewHolder(@NonNull final MyHolder holder, int position) {
        holder.bindData(creditCards.get(position),mContext,mActivity);

        holder.btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialg();
                Thread threadA = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        creditCards.get(holder.getAdapterPosition()).setmContext(mContext);
                        answer = creditCards.get(holder.getAdapterPosition()).Del();
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (answer){
                                    mActivity.obtainCreditCards();
                                }else {
                                    Toast.makeText(mContext,"No se pudo eliminar",Toast.LENGTH_SHORT).show();
                                }
                                closeDialog();

                            }
                        });
                    }
                });

                threadA.start();
            }
        });
    }

    @Override
    public int getItemCount() {
        return creditCards.size();
    }

    private void closeDialog() {
        dialogWait.dismiss();
    }

    private void showDialg() {
        dialogWait = new DialogWait();
        dialogWait.setCancelable(false);
        dialogWait.show(((FragmentActivity)mActivity).getSupportFragmentManager(),"Espera");
    }
}
