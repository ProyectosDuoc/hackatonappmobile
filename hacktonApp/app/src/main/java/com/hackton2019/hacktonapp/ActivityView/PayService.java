package com.hackton2019.hacktonapp.ActivityView;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.hackton2019.hacktonapp.ActivityView.ServiceParking.parkimetroActivity;
import com.hackton2019.hacktonapp.ConnectionServices.TinyDB;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogComment;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogYesNo;
import com.hackton2019.hacktonapp.R;

public class PayService extends AppCompatActivity {

    MaterialButton btnPay;
    TextView textDate,textPH,textTime,textTotal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pay_service);
        init();

        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(),"hola",Toast.LENGTH_SHORT).show();


                DialogComment dialogInfo = new DialogComment();
                dialogInfo.setCancelable(false);
                dialogInfo.show(getSupportFragmentManager(),"comment");
            }
        });
    }

    private void init() {

        textDate = findViewById(R.id.textDate);
        textPH = findViewById(R.id.costByHour);
        textTime = findViewById(R.id.TotalTime);
        textTotal = findViewById(R.id.total);

        TinyDB tinyDB = new TinyDB(getApplicationContext());
        textDate.setText(tinyDB.getString("fechaArriendo"));
        textTime.setText(tinyDB.getString("Time"));
        textPH.setText(tinyDB.getString("PerHour"));
        textTotal.setText(tinyDB.getString("Total"));
        btnPay = findViewById(R.id.btnPay);
    }

    @Override
    public void onBackPressed() {

    }
}
