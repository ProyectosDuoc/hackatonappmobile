package com.hackton2019.hacktonapp.ActivityView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.material.button.MaterialButton;
import com.hackton2019.hacktonapp.ActivityView.LoginUser.FragmentSMSLogin;
import com.hackton2019.hacktonapp.ActivityView.LoginUser.FragmentUserLogin;
import com.hackton2019.hacktonapp.ActivityView.NewUser.FragmentNewProfile;
import com.hackton2019.hacktonapp.ActivityView.NewUser.FragmentNewSMS;
import com.hackton2019.hacktonapp.ActivityView.NewUser.FragmentNewUser;
import com.hackton2019.hacktonapp.ClasesServices.UsersClass.User;
import com.hackton2019.hacktonapp.MainActivity;
import com.hackton2019.hacktonapp.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainLoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[] {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_WIFI_STATE};

    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;

    //**Fragments for New Users
    FragmentNewUser newUser = new FragmentNewUser();

    //**Fragments for register Users
    FragmentUserLogin loginUser = new FragmentUserLogin();

    //**Items in view
    RelativeLayout principalView,secondaryView;
    LinearLayout containerFragment;

    MaterialButton btnLogin,btnNewUser,btnBack;

    //**Controllers
    FragmentTransaction transaction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_main_login);
        checkPermissions();
        init();

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onClick(View view) {
        transaction = getSupportFragmentManager().beginTransaction();
        switch (view.getId()){
            case R.id.btnLoginUser:
                principalView.setVisibility(View.GONE);
                secondaryView.setVisibility(View.VISIBLE);
                transaction.show(loginUser);
                transaction.replace(R.id.containerItems,loginUser);
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.addToBackStack(null);
                transaction.commit();
                break;
            case R.id.btnNewUser:
                principalView.setVisibility(View.GONE);
                secondaryView.setVisibility(View.VISIBLE);
                transaction.show(newUser);
                transaction.replace(R.id.containerItems,newUser);
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.addToBackStack(null);
                transaction.commit();
                break;
            case R.id.btnCancel:
                principalView.setVisibility(View.VISIBLE);
                secondaryView.setVisibility(View.GONE);
                for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                    getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                }
                break;

        }
    }

    private void init() {
        btnLogin = findViewById(R.id.btnLoginUser);
        btnLogin.setOnClickListener(this);
        btnNewUser = findViewById(R.id.btnNewUser);
        btnNewUser.setOnClickListener(this);
        btnBack = findViewById(R.id.btnCancel);
        btnBack.setOnClickListener(this);

        containerFragment = findViewById(R.id.containerItems);
        principalView = findViewById(R.id.principalView);
        principalView.setVisibility(View.VISIBLE);
        secondaryView = findViewById(R.id.secondaryView);
    }

    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS,
                    grantResults);
        }
    }
}
