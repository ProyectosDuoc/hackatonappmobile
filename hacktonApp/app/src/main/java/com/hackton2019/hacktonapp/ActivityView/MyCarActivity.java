package com.hackton2019.hacktonapp.ActivityView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.hackton2019.hacktonapp.ClasesServices.UtilitiesClass.LicencePlate;
import com.hackton2019.hacktonapp.HelperClass.Adapters.MyCarAdapter;
import com.hackton2019.hacktonapp.HelperClass.Adapters.ParkingListAdapter;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogNewCar;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogWait;
import com.hackton2019.hacktonapp.HelperClass.Navigation;
import com.hackton2019.hacktonapp.R;

import java.util.ArrayList;
import java.util.List;

public class MyCarActivity extends AppCompatActivity {

    public RecyclerView mRecycleView;
    public MyCarAdapter mAdapter;
    public RecyclerView.LayoutManager mLayoutManager;

    List<LicencePlate> licenses;

    MaterialButton btnBack;

    FloatingActionButton btnAdd;


    LicencePlate licencePlate;


    private DialogNewCar dialogNewCar;


    private DialogWait dialogNWait;
    boolean answer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_car_activity);
        init();
        obtainLicences();


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.navegate(Navigation.MAP_ACTIVITY,Navigation.FLAG_FULL,getApplicationContext());
                finish();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialg();
            }
        });
    }

    public void obtainLicences() {

        showWait();
        Thread threadA = new Thread(new Runnable() {
            @Override
            public void run() {
                licencePlate = new LicencePlate();
                licencePlate.setmContext(getApplicationContext());
                answer = licencePlate.Read();
                MyCarActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (answer){
                            licenses = licencePlate.getAutosLista();
                            builderList();
                            closeWait();
                        }
                    }
                });
            }
        });

        threadA.start();
    }

    private void init() {
        licenses = new ArrayList<>();
        mRecycleView = findViewById(R.id.licencesRecyclerView);

        btnBack = findViewById(R.id.btnBack);
        btnAdd = findViewById(R.id.btnAddPatente);
    }


    private void builderList() {
        mRecycleView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mAdapter = new MyCarAdapter(licenses,getApplicationContext(),MyCarActivity.this);

        mRecycleView.setAdapter(mAdapter);
        mRecycleView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void onBackPressed() {
        Navigation.navegate(Navigation.MAP_ACTIVITY,Navigation.FLAG_FULL,getApplicationContext());
        finish();
    }

    private void closeDialog() {
        dialogNewCar.dismiss();
    }

    private void showDialg() {
        dialogNewCar = new DialogNewCar();
        dialogNewCar.mActivity = MyCarActivity.this;
        dialogNewCar.setCancelable(false);
        dialogNewCar.show(getSupportFragmentManager(),"Patente");
    }

    private void closeWait() {
        dialogNWait.dismiss();
    }

    private void showWait() {
        dialogNWait = new DialogWait();
        dialogNWait.setCancelable(false);
        dialogNWait.show(getSupportFragmentManager(),"Patente");
    }
}
