package com.hackton2019.hacktonapp.ActivityView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.hackton2019.hacktonapp.ClasesServices.UtilitiesClass.CreditCard;
import com.hackton2019.hacktonapp.ClasesServices.UtilitiesClass.LicencePlate;
import com.hackton2019.hacktonapp.HelperClass.Adapters.CreditCardAdapter;
import com.hackton2019.hacktonapp.HelperClass.Adapters.MyCarAdapter;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogWait;
import com.hackton2019.hacktonapp.HelperClass.Navigation;
import com.hackton2019.hacktonapp.R;

import java.util.ArrayList;
import java.util.List;

public class CreditCardActivity extends AppCompatActivity {


    public RecyclerView mRecycleView;
    public CreditCardAdapter mAdapter;
    public RecyclerView.LayoutManager mLayoutManager;

    List<CreditCard> creditCardList;

    MaterialButton btnBack;

    FloatingActionButton btnAddCard;
    CreditCard creditCard;

    private  boolean answer;
    private DialogWait dialogNWait;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.credit_card_activity);
        init();
        obtainCreditCards();

        btnAddCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newCard();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.navegate(Navigation.MAP_ACTIVITY,Navigation.FLAG_FULL,getApplicationContext());
            }
        });
    }

    public void obtainCreditCards() {
        showWait();
        Thread threadA = new Thread(new Runnable() {
            @Override
            public void run() {
                creditCard = new CreditCard();
                creditCard.setmContext(getApplicationContext());
                answer = creditCard.Read();
                CreditCardActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (answer){
                            creditCardList = creditCard.getListCards();
                            builderList();
                            closeWait();
                        }else {
                            closeWait();
                            obtainCreditCards();
                        }
                    }
                });
            }
        });

        threadA.start();
    }

    private void newCard() {
        Navigation.navegate(Navigation.NEW_CARD_ACTIVITY,Navigation.FLAG_FULL,getApplicationContext());
    }


    @Override
    public void onBackPressed() {
        Navigation.navegate(Navigation.MAP_ACTIVITY,Navigation.FLAG_FULL,getApplicationContext());
    }

    private void init() {
        creditCardList = new ArrayList<>();
        creditCard = new CreditCard();
        btnBack = findViewById(R.id.btnBack);

        btnAddCard = findViewById(R.id.btnAddNewCard);
    }

    private void builderList() {
        mRecycleView = findViewById(R.id.credit_card_recyclerView);
        mRecycleView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mAdapter = new CreditCardAdapter(creditCardList,getApplicationContext(),CreditCardActivity.this);

        mRecycleView.setAdapter(mAdapter);
        mRecycleView.setLayoutManager(mLayoutManager);
    }

    private void closeWait() {
        dialogNWait.dismiss();
    }

    private void showWait() {
        dialogNWait = new DialogWait();
        dialogNWait.setCancelable(false);
        dialogNWait.show(getSupportFragmentManager(),"Patente");
    }

}
