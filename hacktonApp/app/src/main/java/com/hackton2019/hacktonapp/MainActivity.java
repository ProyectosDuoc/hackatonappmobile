package com.hackton2019.hacktonapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.hackton2019.hacktonapp.ActivityView.MyCarActivity;
import com.hackton2019.hacktonapp.ClasesServices.ParkingsClass.Parking;
import com.hackton2019.hacktonapp.ClasesServices.ParkingsClass.ParkingList;
import com.hackton2019.hacktonapp.ClasesServices.UtilitiesClass.CreditCard;
import com.hackton2019.hacktonapp.ConnectionServices.TinyDB;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.BottomSheetListParking;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogAskinfForCard;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogInfo;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogNewCar;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogParkingInfo;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogRegistro;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogSelectedCard;
import com.hackton2019.hacktonapp.HelperClass.Navigation;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.common.ViewObject;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapCircle;
import com.here.android.mpa.mapping.MapGesture;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapObject;
import com.here.android.mpa.mapping.SupportMapFragment;
import com.location.aravind.getlocation.GeoLocator;

import com.nightonke.boommenu.Animation.BoomEnum;
import com.nightonke.boommenu.Animation.OrderEnum;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.HamButton;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ir.drax.netwatch.NetWatch;
import ir.drax.netwatch.cb.NetworkChangeReceiver_navigator;


public class MainActivity extends AppCompatActivity implements PositioningManager.OnPositionChangedListener, View.OnClickListener {
    /**
     * permissions request code
     */
    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;

    /**
     * Permissions that need to be explicitly requested from end user.
     */
    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[] {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_WIFI_STATE};

    /**
     * PositionManager to specified the point in the map
     */
    private PositioningManager mPositioningManager;
    /**
     * Map to be use
     */
    private Map map = null;

    /**
     * To call the map into the fragment
     */
    private SupportMapFragment mapFragment = null;

    /**
     * To put parking into map
     */
    private MapMarker marker = null;

    /**
     * Circle to find near parkings
     */
    MapCircle circle;

    /**
     * To find geolocalization
     */
    GeoLocator geoLocator;

    /**
     * To manage parking List into the Activity
     */
    ParkingList parkingList;

    /**
     * Bool to know if fech found
     */
    boolean answerFetch = false;

    /**
     * Map Object to save markers
     */
    List<MapObject> markers = new ArrayList<>();

    /**
     * Icons for Parkings
     */
    Image open_icon = new Image();
    Image use_icon = new Image();
    Image far_icon = new Image();
    Image lock_icon = new Image();

    /**
     * Buttons On MAP
     */
    FloatingActionButton btnLocate,btnInfo,btnSheet;

    /**
     * Buttons Animate
     */
    BoomMenuButton bmb;

    /**
     * Timer to count when the time is ready
     */
    CountDownTimer counter = null;

    /**
     * Threads for work in background
     */
    Thread threadA,threadB;

    /**
     * To indicate app start first time
     */
    int firstTIme = 1;

    /**
     * To get position in map
     */
    public static GeoPosition geoPositionCaptured;

    /**
     * SpeedInfo
     */
    TextView speedText;

    /**
     * bottom sheet
     */
    BottomSheetListParking bottomSheet;


    CreditCard card ;
    List<CreditCard> credits;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkPermissions();
        checkSession();

    }

    public void askforRegister() {
        new CountDownTimer(10000, 1000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                DialogRegistro dialogInfo = new DialogRegistro();
                dialogInfo.mActivity = MainActivity.this;
                dialogInfo.setCancelable(false);
                dialogInfo.show(getSupportFragmentManager(), "Info Parking");
            }
        }.start();
    }


    /**
     * Main Method OncClick Listeners
     * @param view
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLocate:
                GeoCoordinate coordinate = new GeoCoordinate(geoPositionCaptured.getCoordinate().getLatitude(), geoPositionCaptured.getCoordinate().getLongitude());
                map.setCenter(coordinate, Map.Animation.LINEAR);

                try {
                    new CountDownTimer(1000, 1000) {
                        public void onTick(long millisUntilFinished) {

                        }

                        public void onFinish() {
                            map.setZoomLevel(16, Map.Animation.LINEAR);
                        }
                    }.start();
                } catch (Exception e) {
                    Log.i("excepcion", "conterTimeToReSend: " + e);
                }
                break;
            case R.id.btnSheetInfo:
                callingBottomSheet();
                break;
            case R.id.btnInfo:
                DialogInfo dialogInfo = new DialogInfo();
                dialogInfo.show(getSupportFragmentManager(),"Info Parking");
                break;
        }
    }

    /**
     * OnPause Listenener
     */
    @Override
    protected void onPause() {
        super.onPause();
        if (mPositioningManager != null) {
            mPositioningManager.stop();
            closeAllProcess();
        }
    }

    /**
     * OnResume Listener
     */
    @Override
    protected void onResume() {
        super.onResume();
        if (mPositioningManager != null) {
            mPositioningManager.start(PositioningManager.LocationMethod.GPS_NETWORK);
        }
    }

    /**
     * OnPosition Update Green Light Point in map
     * @param locationMethod
     * @param geoPosition
     * @param b
     */
    @Override
    public void onPositionUpdated(PositioningManager.LocationMethod locationMethod, final GeoPosition geoPosition, boolean b) {
        geoPositionCaptured = geoPosition;
        //Log.i("speed", "onPositionUpdated: "+Math.round(geoPosition.getSpeed()*3600)/1000);
        speedText.setText(getResources().getString(R.string.Speed)+"\n"+Math.round(geoPosition.getSpeed()*3600)/1000);
        int speed = (int) Math.round((geoPosition.getSpeed()*3600)/1000);
        if ( speed >= 20){
            speedText.setBackground(getDrawable(R.drawable.disponible_no_shape));
            GeoCoordinate coordinate = new GeoCoordinate(geoPosition.getCoordinate().getLatitude(),geoPosition.getCoordinate().getLongitude());
            map.setCenter(coordinate,Map.Animation.LINEAR);
        }else {
            speedText.setBackground(getDrawable(R.drawable.disponible_shape));
        }
        circle.setCenter(geoPosition.getCoordinate());

        map.addMapObject(circle);

        if (firstTIme == 1){
            getFirstOpenMarkerPosition();

        }else {
            getMarkerPosition();
        }




    }


    @Override
    public void onPositionFixChanged(PositioningManager.LocationMethod locationMethod, PositioningManager.LocationStatus locationStatus) {

    }

    /**
     * Request permision app for list Manifest Variable
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                for (int index = permissions.length - 1; index >= 0; --index) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        // exit the app if one permission is not granted
                        Toast.makeText(this, "Required permission '" + permissions[index]
                                + "' not granted, exiting", Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }
                }
                // all permissions were granted
                initializeMap();
                break;
        }
    }



    /**
     *
     * Methods no Override
     *
     */

    private void callingBottomSheet() {
        bottomSheet = BottomSheetListParking.newInstance();
        bottomSheet.mActivity = MainActivity.this;
        bottomSheet.setStyle(BottomSheetDialogFragment.STYLE_NO_FRAME, R.style.SheetDialog);
        bottomSheet.show(getSupportFragmentManager(), "bottomSheet");
    }

    private void getFirstOpenMarkerPosition() {
        threadB = new Thread(new Runnable() {
            @Override
            public void run() {
                parkingList = new ParkingList();
                answerFetch = parkingList.fetchList(getApplicationContext(),geoPositionCaptured);
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (answerFetch){
                            map.removeMapObjects(markers);
                            markers.clear();
                            Log.i("number", "run: "+parkingList.getParkings().size());

                            for (int i = 0; i < parkingList.getParkings().size(); i++) {
                                Log.i("parkingListBlock", "onConnected: "+parkingList.getParkingsLock().size());
                                if (parkingList.getParkings().get(i).isUse()){
                                    marker = new MapMarker(new GeoCoordinate(parkingList.getParkings().get(i).getLatittude(), parkingList.getParkings().get(i).getLongitude()),open_icon);
                                    marker.setTitle(parkingList.getParkings().get(i).getId());
                                }else if (!parkingList.getParkings().get(i).isUse()){
                                    marker = new MapMarker(new GeoCoordinate(parkingList.getParkings().get(i).getLatittude(), parkingList.getParkings().get(i).getLongitude()),use_icon);
                                    marker.setTitle(parkingList.getParkings().get(i).getId());
                                }
                                markers.add(marker);
                            }

                            for (int i = 0; i < parkingList.getParkingsFar().size(); i++) {
                                marker = new MapMarker(new GeoCoordinate(parkingList.getParkingsFar().get(i).getLatittude(), parkingList.getParkingsFar().get(i).getLongitude()),far_icon);
                                marker.setTitle(parkingList.getParkingsFar().get(i).getId());
                                markers.add(marker);
                            }

                            for (int i = 0; i < parkingList.getParkingsLock().size(); i++) {
                                marker = new MapMarker(new GeoCoordinate(parkingList.getParkingsLock().get(i).getLatittude(), parkingList.getParkingsLock().get(i).getLongitude()),lock_icon);
                                marker.setTitle(parkingList.getParkingsLock().get(i).getId());
                                markers.add(marker);
                            }
                            parkingList = null;
                            map.addMapObjects(markers);

                        }

                    }

                });
            }
        });

        threadB.start();
        firstTIme = 0;
    }

    private void getMarkerPosition() {
        /**
         * Thread Work in background to find parkings
         */
        if (counter == null){
            counter = new CountDownTimer(20000, 1000) {
                @Override
                public void onTick(long l) {
                    //Toast.makeText(getApplicationContext(),""+l / 1000,Toast.LENGTH_SHORT).show();
                }
                @Override
                public void onFinish() {

                    threadA = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            parkingList = new ParkingList();
                            answerFetch = parkingList.fetchList(getApplicationContext(),geoPositionCaptured);
                            MainActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (answerFetch){
                                        map.removeMapObjects(markers);
                                        markers.clear();
                                        Log.i("number", "run: "+parkingList.getParkings().size());

                                        for (int i = 0; i < parkingList.getParkings().size(); i++) {

                                            Log.i("parkingListBlock", "onConnected: "+parkingList.getParkingsLock().size());
                                            if (parkingList.getParkings().get(i).isUse()){
                                                marker = new MapMarker(new GeoCoordinate(parkingList.getParkings().get(i).getLatittude(), parkingList.getParkings().get(i).getLongitude()),open_icon);
                                                marker.setTitle(parkingList.getParkings().get(i).getId());
                                            }else if (!parkingList.getParkings().get(i).isUse()){
                                                marker = new MapMarker(new GeoCoordinate(parkingList.getParkings().get(i).getLatittude(), parkingList.getParkings().get(i).getLongitude()),use_icon);
                                                marker.setTitle(parkingList.getParkings().get(i).getId());
                                            }
                                            markers.add(marker);
                                        }

                                        for (int i = 0; i < parkingList.getParkingsFar().size(); i++) {
                                            marker = new MapMarker(new GeoCoordinate(parkingList.getParkingsFar().get(i).getLatittude(), parkingList.getParkingsFar().get(i).getLongitude()),far_icon);
                                            marker.setTitle(parkingList.getParkingsFar().get(i).getId());
                                            markers.add(marker);
                                        }

                                        for (int i = 0; i < parkingList.getParkingsLock().size(); i++) {
                                            marker = new MapMarker(new GeoCoordinate(parkingList.getParkingsLock().get(i).getLatittude(), parkingList.getParkingsLock().get(i).getLongitude()),lock_icon);
                                            marker.setTitle(parkingList.getParkingsLock().get(i).getId());
                                            markers.add(marker);
                                        }
                                        parkingList = null;
                                        map.addMapObjects(markers);

                                    }

                                }
                            });
                        }
                    });

                    threadA.start();

                    counter = null;
                }
            }.start();

        }
    }

    /**
     * Check if exist any session from user
     */
    private void checkSession() {
        try {
            initIcon();
        } catch (IOException e) {
            e.printStackTrace();
        }
        geoLocator = new GeoLocator(getApplicationContext(),MainActivity.this);
        initializeMap();
        initController();
        askingCreditCard();
        TinyDB tinyDB = new TinyDB(getApplicationContext());
        if (tinyDB.getString("token")==null || tinyDB.getString("token").isEmpty()){
            btnSheet.setVisibility(View.GONE);
            bmb.setVisibility(View.GONE);
            askforRegister();
            //Navigation.navegate(Navigation.LOGIN_ACTIVITY,Navigation.FLAG_FULL,getApplicationContext());
        }
    }

    private void askingCreditCard() {
        card = new CreditCard();
        credits = new ArrayList<>();

        Thread threadA = new Thread(new Runnable() {
            @Override
            public void run() {
                card.setmContext(getApplicationContext());
               card.Read();
               MainActivity.this.runOnUiThread(new Runnable() {
                   @Override
                   public void run() {
                       if (card.getListCards().size()<=0){
                           DialogAskinfForCard dialogInfo = new DialogAskinfForCard();
                           dialogInfo.setCancelable(false);
                           dialogInfo.show(getSupportFragmentManager(), "Info Parking");
                       }
                   }
               });
            }
        });
        threadA.start();

    }

    /**
     * Initialited Controllers
     */
    private void initController() {
        btnLocate = findViewById(R.id.btnLocate);
        btnLocate.setOnClickListener(this);
        btnInfo = findViewById(R.id.btnInfo);
        btnInfo.setOnClickListener(this);
        btnSheet = findViewById(R.id.btnSheetInfo);
        btnSheet.setOnClickListener(this);

        speedText = findViewById(R.id.textSpeedInfo);

        bmb = findViewById(R.id.bmb);
        settingUserPlace();
    }

    /**
     * Button Menu Settings and Listeners.
     */
    private void settingUserPlace() {
        String Texto = null;
        Drawable drawable = null;
        int color = 0;

        //region Configuracion Boton Menu
        bmb.setButtonEnum(ButtonEnum.Ham);
        bmb.setPiecePlaceEnum(PiecePlaceEnum.HAM_6);
        //bmb.setBackgroundEffect(false);
        bmb.setButtonPlaceEnum(ButtonPlaceEnum.HAM_6);
        bmb.setOrderEnum(OrderEnum.RANDOM);
        bmb.setBoomEnum(BoomEnum.HORIZONTAL_THROW_1);
        //endregion Configuracion Boton Menu

        //region Ciclo Boton Menu
        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++) {
            switch (i) {
                case 0:
                    //Profile
                    Texto = getResources().getString(R.string.profile);
                    drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_account_circle_white_48dp);
                    color = ContextCompat.getColor(getApplicationContext(), R.color.default_bmb_highlighted_color);
                    break;
                case 1:
                    //Mode to owner
                    Texto = getResources().getString(R.string.owner);
                    drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_swap_vertical_circle_white_48dp);
                    color = ContextCompat.getColor(getApplicationContext(), R.color.default_bmb_highlighted_color);
                    break;
                case 2:
                    //payment
                    Texto = getResources().getString(R.string.payment);
                    drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_credit_card_white_48dp);
                    color = ContextCompat.getColor(getApplicationContext(), R.color.default_bmb_highlighted_color);
                    break;
                case 3:
                    //cars
                    Texto = getResources().getString(R.string.cars);;
                    drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_directions_car_white_48dp);
                    color = ContextCompat.getColor(getApplicationContext(), R.color.default_bmb_highlighted_color);
                    break;
                case 4:
                    //about
                    Texto = getResources().getString(R.string.about_us);
                    drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_copyright_white_48dp);
                    color = ContextCompat.getColor(getApplicationContext(), R.color.default_bmb_highlighted_color);
                    break;
                case 5:
                    //log out
                    Texto = getResources().getString(R.string.log_out);
                    drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_exit_to_app_white_48dp);
                    color = ContextCompat.getColor(getApplicationContext(), R.color.default_bmb_highlighted_color);
                    break;
            }
            HamButton.Builder builder = new HamButton.Builder()
                    .listener(new OnBMClickListener() {
                        @Override
                        public void onBoomButtonClick(int index) {
                            // When the boom-button corresponding this builder is clicked.


                            switch (index) {
                                case 0:
                                    //Perfil de usuario

                                    Navigation.navegate(Navigation.PROFILE_ACTIVITY,Navigation.FLAG_BACK,getApplicationContext());
                                    break;
                                case 1:
                                    //Modo de uso

                                    Navigation.navegate(Navigation.OWNER_ACTIVITY,Navigation.FLAG_BACK,getApplicationContext());
                                    break;
                                case 2:
                                    //credit Card

                                    Navigation.navegate(Navigation.PAYMENT_ACTIVITY,Navigation.FLAG_BACK,getApplicationContext());
                                    break;
                                case 3:
                                    //cars

                                    Navigation.navegate(Navigation.CARS_ACTIVITY,Navigation.FLAG_BACK,getApplicationContext());
                                    break;
                                case 4:
                                    //Nosotros

                                    Navigation.navegate(Navigation.ABOUT_ACTIVITY,Navigation.FLAG_BACK,getApplicationContext());
                                    break;
                                case 5:
                                    //Cerrar Sesion

                                    TinyDB tinyDB = new TinyDB(getApplicationContext());
                                    tinyDB.putString("token","");

                                    Navigation.navegate(Navigation.LOGIN_ACTIVITY,Navigation.FLAG_FULL,getApplicationContext());
                                    break;
                            }

                            try {
                                closeAllProcess();

                            }catch (Exception e){

                            }
                            finish();


                        }
                    }).normalImageDrawable(drawable)
                    .normalText(Texto)
                    .normalColor(color);
            bmb.addBuilder(builder);
        }
        //endregion Ciclo Boton Menu

    }

    private void closeAllProcess() {
        try {
            threadA.interrupt();
            threadB.interrupt();
            counter.cancel();
        }catch (Exception e){

        }

    }

    /**
     * Initializer maps
     */
    private void initializeMap() {

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapfragment);

        Log.i("mapa", "initialize: mapa");
        mapFragment.init(new OnEngineInitListener() {
            @Override
            public void onEngineInitializationCompleted(OnEngineInitListener.Error error) {
                if (error == OnEngineInitListener.Error.NONE) {
                    map = mapFragment.getMap();
                    map.setZoomLevel(16,Map.Animation.LINEAR);
                    //marker = new MapMarker(new GeoCoordinate(geoLocator.getLattitude(), geoLocator.getLongitude()));
                    //map.addMapObject(marker);
                    mPositioningManager = PositioningManager.getInstance();
                    mPositioningManager.addListener(new WeakReference<PositioningManager.OnPositionChangedListener>(MainActivity.this));

                    if (mPositioningManager.start(PositioningManager.LocationMethod.GPS_NETWORK)){
                        map.getPositionIndicator().setVisible(true);
                        mapCircleCreator();
                    }else {
                        Toast.makeText(getApplicationContext(),"Algo No funciono",Toast.LENGTH_SHORT).show();
                    }


                    mapFragment.getMapGesture().addOnGestureListener(onGestureListener);



                } else {
                    Toast.makeText(getApplicationContext(),"no hay mapa",Toast.LENGTH_SHORT).show();
                    //System.out.println("ERROR: Cannot initialize Map Fragment");
                }
            }
        });
    }

    /**
     * Map Circle Creator
     */
    private void mapCircleCreator() {

        circle = new MapCircle(500,map.getCenter());
        circle.setLineColor(Color.BLUE);
        circle.setFillColor(Color.argb(155,18,156,149));;
        circle.setLineWidth(2);

    }

    /**
     * Init Icon Resources
     */
    private void initIcon() throws IOException {
        use_icon.setImageResource(R.drawable.parkingsec);
        open_icon.setImageResource(R.drawable.parkingudsec);
        far_icon.setImageResource(R.drawable.parkingfar);
        lock_icon.setImageResource(R.drawable.parkinglock);
    }

    /**
     * Permissions for users
     */
    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS,
                    grantResults);
        }
    }

    /**
     * Gesture on maps to interact with items in it
     */
    MapGesture.OnGestureListener onGestureListener = new MapGesture.OnGestureListener.OnGestureListenerAdapter() {
        @Override
        public void onPanStart() {
            super.onPanStart();
        }

        @Override
        public void onPanEnd() {
            super.onPanEnd();
        }

        @Override
        public void onMultiFingerManipulationStart() {
            super.onMultiFingerManipulationStart();
        }

        @Override
        public void onMultiFingerManipulationEnd() {
            super.onMultiFingerManipulationEnd();
        }

        @Override
        public boolean onMapObjectsSelected(List<ViewObject> list) {


            for (ViewObject viewObject : list) {
                if (viewObject.getBaseType() == ViewObject.Type.USER_OBJECT) {
                    MapObject mapObject = (MapObject) viewObject;

                    if (mapObject.getType() == MapObject.Type.MARKER) {
                        TinyDB tinyDB = new TinyDB(getApplicationContext());
                        MapMarker window_marker = ((MapMarker) mapObject);

                        System.out.println("Title is................."+window_marker.getTitle());

                        tinyDB.putDouble("markerLat",window_marker.getCoordinate().getLatitude());
                        tinyDB.putDouble("markerLng",window_marker.getCoordinate().getLongitude());
                        tinyDB.putString("parkingID",window_marker.getTitle());

                        DialogParkingInfo dialogInfo = new DialogParkingInfo();
                        dialogInfo.show(getSupportFragmentManager(),"Info Parking");


                        return false;
                    }
                }
            }
            return super.onMapObjectsSelected(list);

        }

        @Override
        public boolean onTapEvent(PointF pointF) {
            return super.onTapEvent(pointF);
        }

        @Override
        public boolean onDoubleTapEvent(PointF pointF) {
            return super.onDoubleTapEvent(pointF);
        }

        @Override
        public void onPinchLocked() {
            super.onPinchLocked();
        }

        @Override
        public boolean onPinchZoomEvent(float v, PointF pointF) {
            return super.onPinchZoomEvent(v, pointF);
        }

        @Override
        public void onRotateLocked() {
            super.onRotateLocked();
        }

        @Override
        public boolean onRotateEvent(float v) {
            return super.onRotateEvent(v);
        }

        @Override
        public boolean onTiltEvent(float v) {
            return super.onTiltEvent(v);
        }

        @Override
        public boolean onLongPressEvent(PointF pointF) {
            return super.onLongPressEvent(pointF);
        }

        @Override
        public boolean onTwoFingerTapEvent(PointF pointF) {
            return super.onTwoFingerTapEvent(pointF);
        }

        @Override
        public void onLongPressRelease() {
            super.onLongPressRelease();
        }
    };


}
