package com.hackton2019.hacktonapp.ActivityView.ServiceParking;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.sliderviewlibrary.SliderView;
import com.google.android.material.button.MaterialButton;
import com.hackton2019.hacktonapp.ActivityView.MyCarActivity;
import com.hackton2019.hacktonapp.ClasesServices.ParkingsClass.Parking;
import com.hackton2019.hacktonapp.ClasesServices.UtilitiesClass.LicencePlate;
import com.hackton2019.hacktonapp.ConnectionServices.TinyDB;
import com.hackton2019.hacktonapp.HelperClass.Adapters.MyCarAdapter;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogNewCar;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogWait;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogYesNo;
import com.hackton2019.hacktonapp.HelperClass.Navigation;
import com.hackton2019.hacktonapp.R;

import java.util.ArrayList;
import java.util.List;

public class BeforeStartConfig extends AppCompatActivity implements View.OnClickListener {

    public RecyclerView mRecycleView;
    public BeforeStartPatenteSelected mAdapter;
    public RecyclerView.LayoutManager mLayoutManager;

    TinyDB tinyDB;
    List<LicencePlate> licenses;
    LicencePlate licencePlate;
    Parking parking;
    ArrayList<String> imagesParking;


    private DialogWait dialogNWait;
    private MaterialButton btnBack;
    private MaterialButton btnStart;
    private MaterialButton btnCancel;
    private SliderView galery;
    DialogYesNo yesNo;

    boolean answer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.before_start_config);
        init();
        obtainLicences();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnBack:
                Navigation.navegate(Navigation.MAP_ACTIVITY,Navigation.FLAG_FULL,getApplicationContext());
                break;
            case R.id.btnStart:
                DialogYesNo dialogInfo = new DialogYesNo();
                dialogInfo.setCancelable(false);
                dialogInfo.show(getSupportFragmentManager(),"SiNO");
                break;
            case R.id.btnCancel:
                Navigation.navegate(Navigation.MAP_ACTIVITY,Navigation.FLAG_FULL,getApplicationContext());
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Navigation.navegate(Navigation.MAP_ACTIVITY,Navigation.FLAG_FULL,getApplicationContext());
    }

    private void init() {
        yesNo = new DialogYesNo();
        tinyDB = new TinyDB(getApplicationContext());
        licenses = new ArrayList<>();
        parking = new Parking();
        parking.setId(tinyDB.getString("parkingID"));
        mRecycleView = findViewById(R.id.licencesRecyclerView);
        galery = findViewById(R.id.before_galeryParking);

        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);
        btnStart = findViewById(R.id.btnStart);
        btnStart.setOnClickListener(this);
        btnCancel = findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(this);
    }


    public void obtainLicences() {

        showWait();
        Thread threadA = new Thread(new Runnable() {
            @Override
            public void run() {
                licencePlate = new LicencePlate();
                imagesParking = parking.parkingDetail(getApplicationContext());
                licencePlate.setmContext(getApplicationContext());
                answer = licencePlate.Read();
                BeforeStartConfig.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (answer && licencePlate.getAutosLista().size()>0){
                            galery.setUrls(imagesParking);
                            licenses = licencePlate.getAutosLista();
                            builderList();
                            closeWait();
                        }else {
                            closeWait();
                                DialogNewCar dialogNewCar = new DialogNewCar();
                                dialogNewCar.mActivityBefore = BeforeStartConfig.this;
                                dialogNewCar.setCancelable(false);
                                dialogNewCar.show(getSupportFragmentManager(),"Patente");

                        }
                    }
                });
            }
        });

        threadA.start();
    }

    private void builderList() {
        mRecycleView.setHasFixedSize(true);

        mLayoutManager = new GridLayoutManager(getApplicationContext(),1,RecyclerView.HORIZONTAL,false);
        mAdapter = new BeforeStartPatenteSelected(licenses,getApplicationContext(),BeforeStartConfig.this);

        mRecycleView.setAdapter(mAdapter);
        mRecycleView.setLayoutManager(mLayoutManager);
    }

    private void closeWait() {
        dialogNWait.dismiss();
    }

    private void showWait() {
        dialogNWait = new DialogWait();
        dialogNWait.setCancelable(false);
        dialogNWait.show(getSupportFragmentManager(),"Patente");
    }


}
