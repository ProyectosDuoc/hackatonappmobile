package com.hackton2019.hacktonapp.ClasesServices.UsersClass;


import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.hackton2019.hacktonapp.ConnectionServices.TinyDB;
import com.hackton2019.hacktonapp.ConnectionServices.Urls;
import com.hackton2019.hacktonapp.ConnectionServices.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class User  {

    /**
     * Token user it will be user for future request to server.
     */
    private String token;

    /**
     * username selected by user
     */
    private String userName;

    /**
     * hide username Unique for every user created
     */
    private String hideUsername;

    /**
     * First name of the user
     */
    private String firstName;

    /**
     * Last name for the user
     */
    private String lastName;

    /**
     * Numberphone from user
     */
    private String number;

    /**
     * user email
     */
    private String email;

    /**
     * code to Validate register via SMS
     */
    private String codeValidation;

    /**
     * StringData image from user
     */
    private String imagenData;

    /**
     * temporal password created by user
     */
    private String passwordUser;

    public User() {
    }

    public User(String token, String userName, String hideUsername, String firstName, String lastName, String number, String email, String codeValidation, String imagenData, String passwordUser) {
        this.token = token;
        this.userName = userName;
        this.hideUsername = hideUsername;
        this.firstName = firstName;
        this.lastName = lastName;
        this.number = number;
        this.email = email;
        this.codeValidation = codeValidation;
        this.imagenData = imagenData;
        this.passwordUser = passwordUser;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getHideUsername() {
        return hideUsername;
    }

    public void setHideUsername(String hideUsername) {
        this.hideUsername = hideUsername;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCodeValidation() {
        return codeValidation;
    }

    public void setCodeValidation(String codeValidation) {
        this.codeValidation = codeValidation;
    }

    public String getImagenData() {
        return imagenData;
    }

    public void setImagenData(String imagenData) {
        this.imagenData = imagenData;
    }

    public String getPasswordUser() {
        return passwordUser;
    }

    public void setPasswordUser(String passwordUser) {
        this.passwordUser = passwordUser;
    }


    /**
     * to answer every request from server.
     */
    private boolean respuesta;

    /**
     * First step from register
     * @param context
     * @return
     */
    public boolean registroEtapaUno(final Context context){
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.REGISTER_STEP_ONE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject Obj ;

                        try {
                            Obj = new JSONObject(response);

                            if (!Obj.getBoolean("success")){
                                Toast.makeText(context,Obj.getString("reason"),Toast.LENGTH_SHORT).show();
                                respuesta = Obj.getBoolean("success");
                                Log.i("Respuesta-Error ", "onResponse: "+respuesta);

                            }else if (Obj.getBoolean("success")){
                                respuesta = Obj.getBoolean("success");
                                Log.i("Respuesta-Bien ", "onResponse: " + respuesta);
                            }
                            Log.i("confirmacion", "onResponse: "+Obj.getBoolean("success"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("errorEtapa1", "onErrorResponse: "+error);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("tel", getNumber());
                params.put("nombre", getFirstName());
                params.put("apellido", getLastName());
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);


        try {

            JSONObject response = future.get(3, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Toast.makeText(context,"",Toast.LENGTH_SHORT).show();
        } catch (ExecutionException e) {
            Toast.makeText(context,"asd3",Toast.LENGTH_SHORT).show();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        return respuesta;

    }

    /**
     * Second step from register
     * @param context
     * @return
     */
    public boolean registroEtapaDosComprobacion(final Context context){
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        respuesta = false;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.REGISTER_VALIDATE_SMS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject Obj ;

                        try {
                            Obj = new JSONObject(response);

                            if (!Obj.getBoolean("success")){
                                Toast.makeText(context,Obj.getString("reason"),Toast.LENGTH_SHORT).show();
                                respuesta = Obj.getBoolean("success");
                                Log.i("Respuesta-Error ", "onResponse: "+respuesta);
                            }else {
                                respuesta = Obj.getBoolean("success");

                                Log.i("Respuesta-Bien ", "onResponse: "+respuesta);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("errorEtapa2", "onErrorResponse: "+error);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("tel", getNumber());
                params.put("nombre", getFirstName());
                params.put("apellido", getLastName());
                params.put("code", getCodeValidation());
                return params;
            }
        }
                ;
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
        Log.i("Respuesta-Final ", "onResponse: "+respuesta);


        try {

            JSONObject response = future.get(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Toast.makeText(context,"asd",Toast.LENGTH_SHORT).show();
        } catch (ExecutionException e) {
            Toast.makeText(context,"asd3",Toast.LENGTH_SHORT).show();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        return respuesta;
    }

    /**
     * Hide login
     * @param context
     * @return
     */
    public boolean registroLoginOculto(final Context context){
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        respuesta = false;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.HIDE_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject Obj ;

                        try {
                            Obj = new JSONObject(response);

                            if (!Obj.getBoolean("success")){
                                Toast.makeText(context,Obj.getString("reason"),Toast.LENGTH_SHORT).show();
                                respuesta = Obj.getBoolean("success");
                                Log.i("LoginOculto-M ", "onResponse: "+respuesta);
                            }else {
                                respuesta = Obj.getBoolean("success");
                                Log.i("LoginOculto-B ", "onResponse: "+respuesta +" "+Obj.getString("token"));


                                setToken(Obj.getString("token"));
                                //Toast.makeText(context,getToken(),Toast.LENGTH_SHORT).show();
                                TinyDB tinyDB = new TinyDB(context);
                                tinyDB.putString("token",getToken());
                                tinyDB.putString("hyj",Obj.getString("hyj"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(context,"Error: "+error,Toast.LENGTH_SHORT).show();
                Log.i("errorRegistroLogin", "onErrorResponse: "+error);
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", getNumber());
                params.put("password", getPasswordUser());
                params.put("code", getCodeValidation());
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);


        try {

            JSONObject response = future.get(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Toast.makeText(context,"asd",Toast.LENGTH_SHORT).show();
        } catch (ExecutionException e) {
            Toast.makeText(context,"asd3",Toast.LENGTH_SHORT).show();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        return respuesta;
    }


    /**
     * Last Step from register
     * @param context
     * @return
     */
    public boolean registroEtapaTresFinal(final Context context){
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        respuesta = false;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.REGISTER_CREATE_USER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject Obj ;

                        try {
                            Obj = new JSONObject(response);

                            if (!Obj.getBoolean("success")){
                                Toast.makeText(context,Obj.getString("reason"),Toast.LENGTH_SHORT).show();
                                respuesta = Obj.getBoolean("success");
                                Log.i("Respuesta-Error ", "onResponse: "+respuesta);

                            }else if (Obj.getBoolean("success")){
                            respuesta = Obj.getBoolean("success");
                            Log.i("Respuesta-Bien ", "onResponse: "+respuesta);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(context,"Error: "+error,Toast.LENGTH_SHORT).show();
                Log.i("errorEtapa3", "onErrorResponse: "+error);

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                TinyDB tinyDB = new TinyDB(context);
                params.put("username", getUserName());
                params.put("contrasenia", getPasswordUser());
                params.put("correo", getEmail());
                params.put("foto", getImagenData());
                params.put("actualhost", Urls.ROOT_URL);
                params.put("Authotization", "token "+tinyDB.getString("token"));
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                TinyDB tinyDB = new TinyDB(context);
                params.put("Authorization", "token "+tinyDB.getString("token"));
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
        Log.i("Respuesta-Final ", "onResponse: "+respuesta);


        try {

            JSONObject response = future.get(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Toast.makeText(context,"asd",Toast.LENGTH_SHORT).show();
        } catch (ExecutionException e) {
            Toast.makeText(context,"asd3",Toast.LENGTH_SHORT).show();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        return respuesta;
    }

    /**
     * Confirm value from SMS
     * @param context
     * @return
     */
    public boolean SMSConfirm(final Context context){
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        respuesta = false;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.SMS_CONFIRM,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject Obj ;

                        try {
                            Obj = new JSONObject(response);

                            if (!Obj.getBoolean("success")){
                                Toast.makeText(context,Obj.getString("reason"),Toast.LENGTH_SHORT).show();
                                respuesta = Obj.getBoolean("success");
                                Log.i("LoginOculto-M ", "onResponse: "+respuesta);
                            }else {
                                respuesta = Obj.getBoolean("success");
                                Log.i("LoginOculto-B ", "onResponse: "+respuesta +" "+Obj.getString("token"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(context,"Error: "+error,Toast.LENGTH_SHORT).show();
                Log.i("errorSMS", "onErrorResponse: "+error);
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", getNumber());
                params.put("password", getPasswordUser());
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);


        try {

            JSONObject response = future.get(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Toast.makeText(context,"asd",Toast.LENGTH_SHORT).show();
        } catch (ExecutionException e) {
            Toast.makeText(context,"asd3",Toast.LENGTH_SHORT).show();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        return respuesta;
    }

    /**
     * To check Email if exist
     * @param context
     * @return
     */
    public boolean EmailCheck(final Context context){
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        respuesta = false;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.EMAIL_VALIDATE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject Obj ;

                        try {
                            Obj = new JSONObject(response);

                            if (!Obj.getBoolean("success")){

                                respuesta = Obj.getBoolean("success");

                            }else {
                                respuesta = Obj.getBoolean("success");

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(context,"Error: "+error,Toast.LENGTH_SHORT).show();
                Log.i("errorSMS", "onErrorResponse: "+error);
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", getEmail());
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);


        try {

            JSONObject response = future.get(5, TimeUnit.SECONDS);
        } catch (InterruptedException ignored) {

        } catch (ExecutionException ignored) {

        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        return respuesta;
    }

    /**
     * to request SMS again
     * @param context
     * @return
     */
    public boolean reSendSMS(final Context context){
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        respuesta = false;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.RE_SEND_SMS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject Obj ;

                        try {
                            Obj = new JSONObject(response);

                            if (!Obj.getBoolean("success")){
                                respuesta = Obj.getBoolean("success");
                            }else {
                                respuesta = Obj.getBoolean("success");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(context,"Error: "+error,Toast.LENGTH_SHORT).show();
                Log.i("errorSMS", "onErrorResponse: "+error);
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("tel", getNumber());
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);


        try {

            JSONObject response = future.get(5, TimeUnit.SECONDS);
        } catch (InterruptedException ignored) {

        } catch (ExecutionException ignored) {

        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        return respuesta;


    }


    public void userDetail(final Context context){
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.GET_USER_DETAIL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject Obj ;
                        Log.i("cliente", "onResponse: "+response);

                        try {
                            Obj = new JSONObject(response);
                            setFirstName(Obj.getString("nombre"));
                            setLastName(Obj.getString("apellido"));
                            setEmail(Obj.getString("correo"));
                            setNumber(Obj.getString("numero"));
                            setImagenData("https://pardob.pythonanywhere.com/"+Obj.getString("imagen"));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"Error: "+error,Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("is_mobile", "yes");

                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                TinyDB tinyDB = new TinyDB(context);
                params.put("Authorization", "token "+tinyDB.getString("token"));
                Log.i("token user", "getHeaders: "+tinyDB.getString("token"));
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);



        try {

            JSONObject response = future.get(3, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Toast.makeText(context,"asd",Toast.LENGTH_SHORT).show();
        } catch (ExecutionException e) {
            Toast.makeText(context,"asd3",Toast.LENGTH_SHORT).show();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

    }

}
