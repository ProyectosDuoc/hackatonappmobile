package com.hackton2019.hacktonapp.HelperClass;

import android.content.Context;
import android.content.Intent;

import com.hackton2019.hacktonapp.ActivityView.AboutUs;
import com.hackton2019.hacktonapp.ActivityView.CreditCardActivity;
import com.hackton2019.hacktonapp.ActivityView.CreditCardWebView;
import com.hackton2019.hacktonapp.ActivityView.GlobalParkingInfo;
import com.hackton2019.hacktonapp.ActivityView.MainLoginActivity;
import com.hackton2019.hacktonapp.ActivityView.MyCarActivity;
import com.hackton2019.hacktonapp.ActivityView.OwnerActivity;
import com.hackton2019.hacktonapp.ActivityView.PayService;
import com.hackton2019.hacktonapp.ActivityView.ServiceParking.BeforeStartConfig;
import com.hackton2019.hacktonapp.ActivityView.UserProfile;
import com.hackton2019.hacktonapp.ActivityView.ServiceParking.parkimetroActivity;
import com.hackton2019.hacktonapp.MainActivity;

public class Navigation {

    public static final int MAP_ACTIVITY = 1;
    public static final int LOGIN_ACTIVITY = 2;
    public static final int PROFILE_ACTIVITY = 3;
    public static final int OWNER_ACTIVITY = 4;
    public static final int PAYMENT_ACTIVITY = 5;
    public static final int CARS_ACTIVITY = 6;
    public static final int ABOUT_ACTIVITY = 7;
    public static final int GLOBAL_INFO_ACTIVITY = 8;
    public static final int PARKIMETRO_ACTIVITY = 9;
    public static final int NEW_CARD_ACTIVITY = 10;
    public static final int BEFORE_ACTIVITY = 11;
    public static final int PAY_ACTIVITY = 12;

    public static final int FLAG_BACK = 1;
    public static final int FLAG_FULL = 2;
    public static final int FLAG_NO = 3;


    public static void navegate(int activity, int flags, Context context){
        Intent navegation = null;
        switch (activity){
            case MAP_ACTIVITY:
                navegation = new Intent(context, MainActivity.class );
                break;
            case PROFILE_ACTIVITY :
                navegation = new Intent(context, UserProfile.class );
                break;
            case OWNER_ACTIVITY :
                navegation = new Intent(context, OwnerActivity.class );
                break;
            case PAYMENT_ACTIVITY :
                navegation = new Intent(context, CreditCardActivity.class );
                break;
            case CARS_ACTIVITY :
                navegation = new Intent(context, MyCarActivity.class );
                break;
            case ABOUT_ACTIVITY :
                navegation = new Intent(context, AboutUs.class );
                break;
            case LOGIN_ACTIVITY :
                navegation = new Intent(context, MainLoginActivity.class );
                break;
            case GLOBAL_INFO_ACTIVITY :
                navegation = new Intent(context, GlobalParkingInfo.class );
                break;
            case PARKIMETRO_ACTIVITY:
                navegation = new Intent(context, parkimetroActivity.class );
                break;
            case NEW_CARD_ACTIVITY:
                navegation = new Intent(context, CreditCardWebView.class );
                break;
            case BEFORE_ACTIVITY:
                navegation = new Intent(context, BeforeStartConfig.class );
                break;
            case PAY_ACTIVITY:
                navegation = new Intent(context, PayService.class );
                break;
        }

        switch (flags){
            case FLAG_FULL:
                navegation.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                break;
            case FLAG_BACK:
                navegation.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                break;
            case FLAG_NO:
                break;
        }

        context.startActivity(navegation);
    }

}
