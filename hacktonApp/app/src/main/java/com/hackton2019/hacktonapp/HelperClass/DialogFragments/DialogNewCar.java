package com.hackton2019.hacktonapp.HelperClass.DialogFragments;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.hackton2019.hacktonapp.ActivityView.MyCarActivity;
import com.hackton2019.hacktonapp.ActivityView.ServiceParking.BeforeStartConfig;
import com.hackton2019.hacktonapp.ClasesServices.UtilitiesClass.LicencePlate;
import com.hackton2019.hacktonapp.HelperClass.Navigation;
import com.hackton2019.hacktonapp.R;

import java.util.Objects;

public class DialogNewCar extends AppCompatDialogFragment {

    private MaterialButton btnAdd;
    private MaterialButton btnCancel;
    private TextInputEditText patenteText;
    private LicencePlate licencePlate;

    private boolean answer;
    public MyCarActivity mActivity;
    public BeforeStartConfig mActivityBefore;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_new_car,null);

        btnAdd = view.findViewById(R.id.btnAddPatente);
        btnCancel = view.findViewById(R.id.btnCancel);
        patenteText = view.findViewById(R.id.textPatente);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                licencePlate = new LicencePlate();
                licencePlate.setPatente(patenteText.getText().toString());
                licencePlate.setmContext(getContext());
                Thread threadA = new Thread(new Runnable() {
                    @Override
                    public void run() {
                      answer =  licencePlate.Create();

                      Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
                          @Override
                          public void run() {
                              if (answer){
                                  if (mActivity == null){
                                      if (mActivityBefore!=null){
                                          mActivityBefore.obtainLicences();
                                      }
                                  }else {
                                      mActivity.obtainLicences();
                                  }

                                  //Navigation.navegate(Navigation.CARS_ACTIVITY,Navigation.FLAG_FULL,getContext());
                                  Objects.requireNonNull(getDialog()).dismiss();

                              }
                          }
                      });

                    }
                });


                threadA.start();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Objects.requireNonNull(getDialog()).dismiss();
                if (mActivity ==null){
                    Navigation.navegate(Navigation.MAP_ACTIVITY,Navigation.FLAG_FULL,getContext());
                }
            }
        });

        builder.setView(view);

        return builder.create();
    }
}
