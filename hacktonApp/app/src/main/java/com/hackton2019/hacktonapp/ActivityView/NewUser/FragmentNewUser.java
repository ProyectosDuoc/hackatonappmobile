package com.hackton2019.hacktonapp.ActivityView.NewUser;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.hackton2019.hacktonapp.ClasesServices.UsersClass.User;
import com.hackton2019.hacktonapp.ConnectionServices.TinyDB;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogWait;
import com.hackton2019.hacktonapp.HelperClass.Navigation;
import com.hackton2019.hacktonapp.R;

public class FragmentNewUser extends Fragment {


    public FragmentNewUser() {
    }

    //**Activity items
    private View mView;
    private Activity mMain;

    //Fragments Items
    private TextInputEditText textPhone;
    private TextInputEditText textLastName;
    private TextInputEditText textFirstName;
    private MaterialButton btnNext;

    private FragmentNewSMS newSMS;
    private DialogWait dialogWait;
    private User user;
    private boolean answer;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.new_user_fragment,container,false);
        mMain = getActivity();
        init();




        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TinyDB tinyDB = new TinyDB(getContext());
                tinyDB.putString("firstname", textFirstName.getText().toString());
                tinyDB.putString("lastname",textLastName.getText().toString());
                tinyDB.putString("number",textPhone.getText().toString());

                user.setFirstName(textPhone.getText().toString());
                user.setLastName(textLastName.getText().toString());
                user.setNumber(textPhone.getText().toString());

                showDialg();
                Thread threadA = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        answer = user.registroEtapaUno(getContext());
                        mMain.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (answer){
                                    closeDialog();
                                    FragmentTransaction fmt = getActivity().getSupportFragmentManager().beginTransaction();
                                    fmt.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                                    fmt.replace(R.id.containerItems,newSMS);
                                    fmt.commit();

                                }else {
                                    closeDialog();
                                    Navigation.navegate(Navigation.LOGIN_ACTIVITY,Navigation.FLAG_FULL,getContext());
                                }
                            }
                        });
                    }
                });
                threadA.start();


            }
        });



        return mView;
    }

    private void init() {
        newSMS = new FragmentNewSMS();
        dialogWait = new DialogWait();
        user = new User();
        textPhone = mView.findViewById(R.id.textNumerPhone);
        textFirstName = mView.findViewById(R.id.textFirstName);
        textLastName = mView.findViewById(R.id.textLastName);

        btnNext = mView.findViewById(R.id.btnNext);
    }

    private void closeDialog() {
        dialogWait.dismiss();
    }

    private void showDialg() {
        dialogWait.setCancelable(false);
        dialogWait.show(getActivity().getSupportFragmentManager(),"Cargando");
    }
}
