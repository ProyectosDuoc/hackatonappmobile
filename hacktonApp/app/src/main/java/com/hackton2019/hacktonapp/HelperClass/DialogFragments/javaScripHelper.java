package com.hackton2019.hacktonapp.HelperClass.DialogFragments;

import android.app.Activity;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

public class javaScripHelper {
    private Activity activity = null;

    public javaScripHelper(Activity activity) {
        this.activity = activity;
    }

    @JavascriptInterface
    public void showMessage(String message) {

        Toast toast = Toast.makeText(this.activity.getApplicationContext(),
                message,
                Toast.LENGTH_SHORT);

        toast.show();
    }
}
