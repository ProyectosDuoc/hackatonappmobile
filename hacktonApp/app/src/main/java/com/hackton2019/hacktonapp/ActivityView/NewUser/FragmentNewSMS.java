package com.hackton2019.hacktonapp.ActivityView.NewUser;

import android.app.Activity;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.button.MaterialButton;
import com.hackton2019.hacktonapp.ClasesServices.UsersClass.User;
import com.hackton2019.hacktonapp.ConnectionServices.TinyDB;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogWait;
import com.hackton2019.hacktonapp.R;

public class    FragmentNewSMS extends Fragment {

    //**Activity Items
    private View mView;
    private Activity mMain;

    //Fragments Items
    private TextView textTimeLeft;
    private TextView textInfoPhone;
    private EditText textSms;
    private MaterialButton btnReSendSMS;

    //Fragment To view
    private FragmentNewProfile newProfile;
    private DialogWait dialogWait;
    private User user;
    private boolean answer;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.new_login_user_sms,container,false);
        mMain = getActivity();
        init();
        watchingSMS();
        conterTimeToReSend();

        btnReSendSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Thread threadB = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        TinyDB tinyDB = new TinyDB(getContext());
                        user.setNumber(tinyDB.getString("number"));
                        user.reSendSMS(getContext());

                        mMain.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btnReSendSMS.setEnabled(false);
                                conterTimeToReSend();
                            }
                        });
                    }
                });
            threadB.start();
            }
        });

        return mView;
    }

    private void watchingSMS() {
        textSms.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                TinyDB tinyDB = new TinyDB(getContext());
                user.setNumber(tinyDB.getString("number"));
                user.setFirstName(tinyDB.getString("firstname"));
                user.setLastName( tinyDB.getString("lastname"));
                user.setCodeValidation(textSms.getText().toString());
                user.setPasswordUser(textSms.getText().toString());
                if (editable.length()==4){
                    showDialg();
                    Thread threadA = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            answer = user.registroEtapaDosComprobacion(getContext());

                            if (answer){

                                Thread threadB = new Thread(new Runnable() {
                                    @Override
                                    public void run() {

                                        answer = user.registroLoginOculto(getContext());

                                        mMain.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (answer){
                                                    closeDialog();
                                                    FragmentTransaction fmt = getActivity().getSupportFragmentManager().beginTransaction();
                                                    fmt.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                                                    fmt.replace(R.id.containerItems,newProfile);
                                                    fmt.commit();
                                                }
                                            }
                                        });
                                        //Activity RunUI
                                    }
                                });
                                threadB.start();
                            }

                        }
                    });
                    threadA.start();


                }
            }
        });
    }

    private void conterTimeToReSend() {
        new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                textTimeLeft.setText("Esperando: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                textTimeLeft.setText("¿Quieres solicitar un nuevo codigo de validación?");
                btnReSendSMS.setEnabled(true);
            }
        }.start();

    }

    private void init() {
        newProfile = new FragmentNewProfile();
        user = new User();
        dialogWait = new DialogWait();
        textInfoPhone = mView.findViewById(R.id.textInfoPhone);
        textTimeLeft = mView.findViewById(R.id.textTimeLeft);


        textSms = mView.findViewById(R.id.textSMS);


        btnReSendSMS = mView.findViewById(R.id.btnReSMS);

        chargeItemsInView();

    }

    private void closeDialog() {
        dialogWait.dismiss();
    }

    private void showDialg() {
        dialogWait.setCancelable(false);
        dialogWait.show(((FragmentActivity)mMain).getSupportFragmentManager(),"Cargando");
    }

    private void chargeItemsInView() {
        TinyDB tinyDB = new TinyDB(getContext());
        textInfoPhone.setText(getResources().getString(R.string.info_phone)+" "+tinyDB.getString("number"));
    }
}
