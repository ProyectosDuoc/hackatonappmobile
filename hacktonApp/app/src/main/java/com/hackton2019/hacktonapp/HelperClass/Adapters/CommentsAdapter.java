package com.hackton2019.hacktonapp.HelperClass.Adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.hackton2019.hacktonapp.ClasesServices.UtilitiesClass.Comments;
import com.hackton2019.hacktonapp.R;

import java.util.List;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.MyHolderComments> {

    private List<Comments> Comments;
    private Context mContext;
    private Activity mActivity;

    public CommentsAdapter() {

    }

    public CommentsAdapter(List<Comments> Comments, Context mContext, Activity mActivity) {

        this.Comments = Comments;
        this.mContext = mContext;
        this.mActivity = mActivity;
    }

    public static class MyHolderComments extends RecyclerView.ViewHolder{

        ImageView imageProfile;
        TextView userName,commentDate,commentText;
        MaterialRatingBar ratingBar;
        public MyHolderComments(@NonNull View itemView) {
            super(itemView);
            imageProfile = itemView.findViewById(R.id.comment_profile_image);
            userName = itemView.findViewById(R.id.comment_textNameUser);
            commentDate = itemView.findViewById(R.id.comment_textDateCommentItem);
            commentText = itemView.findViewById(R.id.comment_CommentItem);
            ratingBar = itemView.findViewById(R.id.comment_ratingBar_item);
        }

        public void bindData(Comments comment, Context context, Activity activity){
            Log.i("url_ImageP", "bindData: "+comment.getUserImage());
            Glide.with(context).load(comment.getUserImage()).into(imageProfile);
            userName.setText(comment.getUser());
            commentDate.setText(comment.getDateComment());
            ratingBar.setRating((float) comment.getNota()/2);
            commentText.setText(comment.getCommentUser());

        }
    }

    @NonNull
    @Override
    public MyHolderComments onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.global_parking_comment_item,parent,false);


        return new CommentsAdapter.MyHolderComments(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolderComments holder, int position) {
        holder.bindData(Comments.get(position),mContext,mActivity);
    }

    @Override
    public int getItemCount() {
        return Comments.size();
    }
}
