package com.hackton2019.hacktonapp.HelperClass.Adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.akash.RevealSwitch;
import com.akash.revealswitch.OnToggleListener;
import com.bumptech.glide.Glide;
import com.example.sliderviewlibrary.SliderView;
import com.google.android.material.button.MaterialButton;
import com.hackton2019.hacktonapp.ClasesServices.ParkingsClass.Parking;
import com.hackton2019.hacktonapp.ConnectionServices.TinyDB;
import com.hackton2019.hacktonapp.HelperClass.DialogFragments.DialogWait;
import com.hackton2019.hacktonapp.R;

import java.util.ArrayList;
import java.util.List;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class ParkingListAdapter extends RecyclerView.Adapter<ParkingListAdapter.MyHolder> {

    private List<Parking> ownerParking;
    private Context mContext;
    private Activity mActivity;

    public ParkingListAdapter() {
    }

    public ParkingListAdapter(List<Parking> ownerParking, Context mContext, Activity mActivity) {
        this.ownerParking = ownerParking;
        this.mContext = mContext;
        this.mActivity = mActivity;
    }

    public static class MyHolder extends RecyclerView.ViewHolder{


        TextView textNameParking,textStateParking;
        MaterialRatingBar ratingBar;
        RevealSwitch parkingSwitch;
        MaterialButton btnInfo;



        public MyHolder(@NonNull View itemView) {
            super(itemView);
            textNameParking = itemView.findViewById(R.id.textNameParking);
            textStateParking = itemView.findViewById(R.id.textStateParking);
            ratingBar = itemView.findViewById(R.id.ratingBar);
            parkingSwitch = itemView.findViewById(R.id.owner_parking_switch);
            btnInfo = itemView.findViewById(R.id.btnInfoParking);

        }

        public void bindData(Parking parking,Context context, Activity activity){
            textNameParking.setText(parking.getName());

            parkingSwitch.setEnable(parking.isLock());
            ratingBar.setRating((float) parking.getStars()/2);
            //Glide.with(context).load(parking.parkingDetail(context).get(1)).into(image);
        }

    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.owner_activity_item,parent,false);

        return new ParkingListAdapter.MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyHolder holder, int position) {
        holder.bindData(ownerParking.get(position),mContext,mActivity);
        initViewElements();


        holder.parkingSwitch.setToggleListener(new OnToggleListener() {
            @Override
            public void onToggle(final boolean b) {

                Thread threadA = new Thread(new Runnable() {
                    @Override
                    public void run() {
                       if (b){
                           parking = new Parking();
                           parking.setId(ownerParking.get(holder.getAdapterPosition()).getId());
                           parking.lockParking(mContext,"lock");
                       }else if (!b){
                           parking = new Parking();
                           parking.setId(ownerParking.get(holder.getAdapterPosition()).getId());
                           parking.lockParking(mContext,"unlock");
                       }else {
                           Log.i("errorLock", "run: Ocurrio un error");
                           Toast.makeText(mContext,"No se pudo completar el proceso",Toast.LENGTH_SHORT).show();
                       }
                       mActivity.runOnUiThread(new Runnable() {
                           @Override
                           public void run() {
                               holder.parkingSwitch.setEnable(b);
                           }
                       });
                    }
                });

                threadA.start();

            }
        });





        holder.btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                visibleContainers();
                showDialg();
                Thread threadA = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        images = ownerParking.get(holder.getAdapterPosition()).parkingDetail(mContext);
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                galeryView.setUrls(images);
                                textNameParking.setText(ownerParking.get(holder.getAdapterPosition()).getName());
                                textDescriptionParking.setText(ownerParking.get(holder.getAdapterPosition()).getDescription());
                                ratingBar.setRating((float) ownerParking.get(holder.getAdapterPosition()).getStars()/2);

                                switchStatusParking.setEnable(ownerParking.get(holder.getAdapterPosition()).isLock());
                                textCalificationParking.setText(String.valueOf(ownerParking.get(holder.getAdapterPosition()).getStars()/2));
                                TinyDB tinyDB = new TinyDB(mContext);
                                tinyDB.putString("parkingID",ownerParking.get(holder.getAdapterPosition()).getId());
                                closeDialog();
                            }
                        });

                    }
                });
                threadA.start();


            }
        });

    }

    @Override
    public int getItemCount() {
        return ownerParking.size();
    }

    private void visibleContainers() {
        containerItem.setVisibility(View.GONE);
        containerInfo.setVisibility(View.VISIBLE);
        containerstatusbar.setVisibility(View.VISIBLE);
    }

    private Parking parking;
    private LinearLayout containerItem,containerInfo,containerstatusbar;
    private SliderView galeryView;
    private TextView textNameParking,textCalificationParking,textDescriptionParking,textStatusBlockParking;
    private RevealSwitch switchStatusParking;
    private MaterialRatingBar ratingBar;
    private ArrayList<String> images;
    private DialogWait dialogWait;
    private void initViewElements() {
        //Containers
        containerItem = mActivity.findViewById(R.id.containerList);
        containerInfo = mActivity.findViewById(R.id.containerItemList);
        containerstatusbar = mActivity.findViewById(R.id.owner_status_bar);

        //elements single Info
        galeryView = mActivity.findViewById(R.id.owner_galeryParking);
        textNameParking = mActivity.findViewById(R.id.owner_NameParking);
        textCalificationParking = mActivity.findViewById(R.id.owner_textReputation);
        textDescriptionParking = mActivity.findViewById(R.id.owner_InfoParking);
        ratingBar = mActivity.findViewById(R.id.owner_ratingBar);

        //elemetns bar single info
        textStatusBlockParking = mActivity.findViewById(R.id.owner_parking_text_lock);
        switchStatusParking = mActivity.findViewById(R.id.owner_main_parking_switch);

        dialogWait = new DialogWait();
    }

    private void closeDialog() {
        dialogWait.dismiss();
    }

    private void showDialg() {
        dialogWait.setCancelable(false);
        dialogWait.show(((AppCompatActivity)mActivity).getSupportFragmentManager(),"Cargando");
    }
}
